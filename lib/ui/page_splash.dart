import 'dart:async';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_ui_collections/utils/utils.dart';
import 'package:myapp/utils/utils.dart';
import '../LocalBindings.dart';
import 'home_viwe.dart';
import 'page_onboarding.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Screen size;

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 4), () {
       navigateFromSplash();
    });
  }

  @override
  Widget build(BuildContext context) {
    
     size = Screen(MediaQuery.of(context).size);
    return Scaffold( backgroundColor: colorCurve,
        body: Center(
          
        child: Container(
           width: size.getWidthPx(300),
           height: size.getWidthPx(300),
          child: FlareActor(
                  "assets/intro.flr",
                  animation: "Untitled",
                  fit: BoxFit.fitHeight,
                  alignment: Alignment.center,
                ),
         // Image.asset("assets/otp.flr")
          )
          )
    );
  }

  Future navigateFromSplash () async {
    String isOnBoard = await LocalStorage.sharedInstance.readValue(Constants.isOnBoard);
      print("isOnBoard  $isOnBoard");
      if(isOnBoard ==null || isOnBoard != "0"){
        //Navigate to OnBoarding Screen.
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => OnBoardingPage()));
      }else{
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeView()));
      }
  }
}