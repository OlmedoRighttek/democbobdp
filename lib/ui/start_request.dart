import 'package:flutter/material.dart';
import 'package:myapp/model/designCourseAppTheme.dart';
import 'package:myapp/model/selectListDat.dart';
import 'package:myapp/ui/page_login.dart';
import 'package:myapp/utils/colors.dart';
import 'package:myapp/utils/colors.dart' as prefix0;
import 'package:myapp/utils/popup_page.dart';
import 'package:myapp/utils/slide_route.dart';
import 'package:localstorage/localstorage.dart';
import 'home_viwe.dart';
import 'package:open_appstore/open_appstore.dart';

class CourseInfoScreen extends StatefulWidget {
  String urlStore = '';
  final HotelListData listPoduct;

  CourseInfoScreen(this.listPoduct);
  @override

  // _CourseInfoScreenState createState() => _CourseInfoScreenState();

    State<StatefulWidget> createState() {
    return _CourseInfoScreenState(this.listPoduct);
  }
}

class _CourseInfoScreenState extends State<CourseInfoScreen>
  with TickerProviderStateMixin {
  final HotelListData listPoduct;
      _CourseInfoScreenState(this.listPoduct);
      final infoHeight = 364.0;
      AnimationController animationController;
      Animation<double> animation;
      var opacity1 = 0.0;
      var opacity2 = 0.0;
      var opacity3 = 0.0;
      bool isAceptarTerminos = true;
     

      final List<int> numbers = [];

 
  final List<String> _listReq = [
    ""
  ];
      final LocalStorage storage = new LocalStorage('todo_app');
  @override
    void initState() {
      animationController = AnimationController(
          duration: Duration(milliseconds: 1000), vsync: this);
      animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
          parent: animationController,
          curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
      setData();
      super.initState();
    }

  void setData() async {
    animationController.forward();
    await Future.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    final tempHight = (MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        24.0);
    return Container(
      color: DesignCourseAppTheme.nearlyWhite,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1.2,
                         // var a = storage.getItem('Img');

                  child: Image.asset('assets/design_course/webInterFace.png'),
                ),
              ],
            ),
            Positioned(
              top: (MediaQuery.of(context).size.width / 1.2) - 24.0,
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                decoration: BoxDecoration(
                  color: DesignCourseAppTheme.nearlyWhite,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32.0),
                      topRight: Radius.circular(32.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: DesignCourseAppTheme.grey.withOpacity(0.2),
                        offset: Offset(1.1, 1.1),
                        blurRadius: 10.0),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  child: SingleChildScrollView(
                    child: Container(
                      constraints: BoxConstraints(
                          minHeight: infoHeight,
                          maxHeight:
                              tempHight > infoHeight ? tempHight : infoHeight),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 32.0, left: 18, right: 16),
                            // child: Text(
                            //   "Bienvenido: Olmedo Zapata",
                            //   textAlign: TextAlign.left,
                            //   style: TextStyle(
                            //     fontWeight: FontWeight.w600,
                            //     fontSize: 18,
                            //     letterSpacing: 0.27,
                            //     color: DesignCourseAppTheme.darkerText,
                            //   ),
                            // ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 16, right: 16, bottom: 8, top: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  listPoduct.titleTxt,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w200,
                                    fontSize: 22,
                                    letterSpacing: 0.27,
                                    color: colorCurve,
                                  ),
                                ),
                                // Container(
                                //   child: Row(
                                //     children: <Widget>[
                                //       Text(
                                //         "4.3",
                                //         textAlign: TextAlign.left,
                                //         style: TextStyle(
                                //           fontWeight: FontWeight.w200,
                                //           fontSize: 22,
                                //           letterSpacing: 0.27,
                                //           color: DesignCourseAppTheme.grey,
                                //         ),
                                //       ),
                                //       Icon(
                                //         Icons.star,
                                //         color: DesignCourseAppTheme.nearlyBlue,
                                //         size: 24,
                                //       ),
                                //     ],
                                //   ),
                                // )
                              ],
                            ),
                          ),
                          
                         cardList(),
                      //  Container(
                      //   padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                      //   height: MediaQuery.of(context).size.height * 0.15,
                      //   child: ListView.builder(
                      //     scrollDirection: Axis.horizontal,
                      //       itemCount: numbers.length, itemBuilder: (context, index) {
                      //         return Container(
                      //           width: MediaQuery.of(context).size.width * 0.3,
                      //           child: Card(
                      //             color: Colors.blue,
                      //             child: Container(
                      //               child: Center(child: Text(numbers[index].toString(), style: TextStyle(color: Colors.white, fontSize: 36.0),)),
                      //             ),
                      //           ),
                      //         );
                      //       })),
                          Expanded(
                            child: AnimatedOpacity(
                              duration: Duration(milliseconds: 500),
                              opacity: opacity2,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 16, right: 16, top: 8, bottom: 8),
                                child: Text(
                                  listPoduct.descriptionInfo,
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w200,
                                    fontSize: 20,
                                    letterSpacing: 0.27,
                                    color: DesignCourseAppTheme.grey,
                                  ),
                                  maxLines: 8,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ),
                          ),
                          AnimatedOpacity(
                            duration: Duration(milliseconds: 500),
                            opacity: opacity3,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 16, bottom: 16, right: 16),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  // Container(
                                  //   width: 48,
                                  //   height: 48,
                                  //   child: Container(
                                  //     decoration: BoxDecoration(
                                  //       color: colorCurve,
                                  //       borderRadius: BorderRadius.all(
                                  //         Radius.circular(16.0),
                                  //       ),
                                  //       border: new Border.all(
                                  //           color: DesignCourseAppTheme.grey
                                  //               .withOpacity(0.2)),
                                  //     ),
                                  //     child: Icon(
                                  //       Icons.money_off,
                                  //       color: Colors.white,
                                  //       size: 28,
                                  //     ),
                                  //   ),
                                  // ),
                                  // SizedBox(
                                  //   width: 16,
                                  // ),
                                  Expanded(
                                    child: Container(
                                      height: 48,
                                      decoration: BoxDecoration(
                                        color: colorCurve,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(20.0),
                                        ),
                                        boxShadow: <BoxShadow>[
                                          BoxShadow(
                                              color: colorCurve
                                                  .withOpacity(0.5),
                                                  
                                              offset: Offset(1.1, 1.1),
                                              blurRadius: 10.0),
                                        ],
                                      ),
                                    
                                       
                                        child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                                borderRadius: new BorderRadius.circular(8.0),
                                      ),
                                          color: colorCurve,
                                          child: Text(
                                         listPoduct.urlStore == '' ? "Empecemos" : "Ir a",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18,
                                            letterSpacing: 0.0,
                                            
                                            color: DesignCourseAppTheme
                                                .nearlyWhite,

                                          ),
                                        ),
                                            onPressed: (){
                                                _validateInputs();
                                            },
                                        ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).padding.bottom,)
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
                                
                                    iconReq(listPoduct.requisitos),
                                    msmReq(listPoduct.requisitos) ,
                              
                                  Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                                    child: SizedBox(
                                      width: AppBar().preferredSize.height,
                                      height: AppBar().preferredSize.height,
                                      child: Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          borderRadius: new BorderRadius.circular(
                                              AppBar().preferredSize.height),
                                          child: Icon(
                                            Icons.arrow_back_ios,
                                            color: DesignCourseAppTheme.nearlyBlack,
                                          ),
                                          onTap: () {
                                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeView()));

                                           // Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));
                                           // Navigator.pop(context, false);
                                          },
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                          
                        }
                      
                          void _validateInputs() {
                            if(listPoduct.urlStore == ''){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
                            }else{
                                var divideUrl =  listPoduct.urlStore.split(' ');
                                OpenAppstore.launch(androidAppId: divideUrl[0], iOSAppId: divideUrl[1]);
                            }
                                    // animationController.reverse().then((data) {
                                    //   setState(() {
                                    //     tabBody =
                                    //         MyDiaryScreen(animationController: animationController);
                                    //         Navigator.push(context, MaterialPageRoute(builder: (context) => MyDiaryScreen(animationController: animationController)));
                      
                                    //   });
                                    // }
                                    
                                    // );
                                
                      
                            // animationController =
                            //   AnimationController(duration: Duration(milliseconds: 600), vsync: this);
                              //  Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
                          }
                      
                      Widget tabBody = Container(
                          color: Colors.red,
                        );
                      
                        Widget getTimeBoxUI(String text1, String txt2) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                color: DesignCourseAppTheme.nearlyWhite,
                                borderRadius: BorderRadius.all(Radius.circular(16.0)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: DesignCourseAppTheme.grey.withOpacity(0.2),
                                      offset: Offset(1.1, 1.1),
                                      blurRadius: 8.0),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      text1,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 14,
                                        letterSpacing: 0.27,
                                        color: colorCurve,
                                      ),
                                    ),
                                    Text(
                                      txt2,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                        fontSize: 14,
                                        letterSpacing: 0.27,
                                        color: DesignCourseAppTheme.grey,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }


    Container cardList(){
      try {
         List<String> media = [];
     

      if(listPoduct.infoCards != ''){

      var element = listPoduct.infoCards.split("|");
      
      for (var i = 0; i < element.length; i++) {
        media.add(element[i]);
      }

       return     Container(
                        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                        height: MediaQuery.of(context).size.height * 0.15,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                            itemCount: element.length, itemBuilder: (context, index) {
                              return Container(
                                decoration: BoxDecoration(
                                //color: DesignCourseAppTheme.nearlyWhite,
                               // borderRadius: BorderRadius.all(Radius.circular(16.0)),
                                // boxShadow: <BoxShadow>[
                                //   BoxShadow(
                                //       color: DesignCourseAppTheme.grey.withOpacity(0.2),
                                //       offset: Offset(1.1, 1.1),
                                //       blurRadius: 8.0),
                                // ],
                              ),
                                width: MediaQuery.of(context).size.width * 0.3,
                                child: Card(
                                  color: Colors.white,
                                  child: Container(
                                    child: Center(child: Text(element[index].toString(), 
                                    style: TextStyle(
                                       fontWeight: FontWeight.w600,
                                        fontSize: 14,
                                        letterSpacing: 0.27,
                                        color: colorCurve,
                                      ),)),
                                  ),
                                ),
                              );
                            }));

        }else {

       return    Container(
                        
                        );
        }
        
      } catch (e) 
      {
          print(e); 

      }
    }






    Positioned msmReq(String requesitos){
     
       if(requesitos != ''){
        return Positioned(
        top: (MediaQuery.of(context).size.width / 1.1) - 20.0 - 0,
        right: 30,
        child: new ScaleTransition(
                alignment: Alignment.center,
                scale: new CurvedAnimation(
                    parent: animationController, curve: Curves.fastOutSlowIn),
                        child:  Text(
                                "Requisitos",
                                    style: TextStyle(
                                        fontFamily: 'Exo2', color: prefix0.colorCurve, fontSize: 15.0),
                                    ),
                                    ),
                          );

       }else{
          return Positioned(child: Text(""),);

       }
     
    }

      Positioned iconReq(String requesitos){
        
      if(requesitos != ''){
        return Positioned(
                top: (MediaQuery.of(context).size.width / 1.2) - 24.0 - 35,
                right: 35,
                child: new GestureDetector(
                                    onTap: (){
                                      Navigator.push(context, SlideTopRoute(page: PopupPage(listPoduct.requisitos)));
                                    },
                                      // alignment: Alignment.center,
                                      // scale: new CurvedAnimation(
                                      // parent: animationController, curve: Curves.fastOutSlowIn),
                                      child: Card(
                                        color: colorCurve,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(50.0)),
                                        elevation: 10.0,
                                        child: Container(
                                          width: 60,
                                          height: 60,
                                          child: Center(
                                            child: Icon(
                                              Icons.assignment,
                                              color: DesignCourseAppTheme.nearlyWhite,
                                              size: 30,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
            );

       }else{
          return Positioned(child: Text(""),);
       }
      }
    }
