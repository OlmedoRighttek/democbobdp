import 'dart:io';

import 'package:flutter/material.dart';
import 'package:myapp/ui/preview.dart';
import 'package:myapp/ui/transaction_steps.dart';
import 'package:myapp/utils/dropdown_formfield.dart';
import 'package:myapp/utils/utils.dart';
import 'package:myapp/utils/utils.dart' as prefix0;
import 'package:myapp/widgets/alert_bdp.dart';
import 'package:myapp/widgets/alert_style.dart';
import 'package:myapp/widgets/constants.dart';
import 'package:myapp/widgets/dialog_button.dart';
import 'package:myapp/widgets/widgets.dart';
import 'package:myapp/utils/responsive_screen.dart';
import 'package:image_picker/image_picker.dart';

import 'package:localstorage/localstorage.dart';




void main() => runApp(new RequestInfoServiceBasic());

class RequestInfoServiceBasic extends StatefulWidget {
  RequestInfoServiceBasic({Key key}) : super(key: key);

  @override
  _RequestPageInfoUser createState() => _RequestPageInfoUser();
}

class _RequestPageInfoUser extends State<RequestInfoServiceBasic> {
//  _formKey and _autoValidate
    final LocalStorage storage = new LocalStorage('todo_app');
 File vZdar;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = true;
  bool flag = true;
  bool _isButtonDisabled = false; //true;
  // String _email;
   String _mobile;
  Screen size;
  String _myActivity;
  File _imageFile;
  dynamic _pickImageError;
  bool _enabled = true;
  bool isCamara = false;

  bool isLoading = false;

  
  @override
  Widget build(BuildContext context) {
        size = Screen(MediaQuery.of(context).size);

   return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Stack(children: <Widget>[
      
    SingleChildScrollView(
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20), vertical: size.getWidthPx(20)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                      children: [ 
                       new  IconButton(
                              icon: Icon(Icons.arrow_back,color: colorCurve),
                              onPressed: () => Navigator.pop(context, false),
                            ), 
                          new Expanded(
                              child: new GradientText ("Ingreso de información del ciente", 
                                      gradient: LinearGradient(colors: [
                                      Color.fromRGBO(0, 94, 184, 1.0),  // fromRGBO(0, 94, 184), 
                                      Color.fromRGBO(0, 42, 102, 1.0)
                                    ]),
                                            style: TextStyle(
                                           fontFamily: 'Exo2', fontSize: 18, fontWeight: FontWeight.bold)
                                  ) ,
                            )
                   ],
                  ),
                    Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: FormUI(),
                        ),
                   ]),
          ),
        ),
      )
    ]));
 

  }

// Here is our Form UI
Widget FormUI() {
    return new Column(
      children: <Widget>[


       _helpX(),
       
       SizedBox(height: 20),

        _selectInfor(),

      SizedBox(height: 20),

        new Theme(
                      child: TextFormField(
                           maxLength: 10,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            // errorText: "ssss",
                            // helperText: "ZIP-Code of shipping destination",
                            labelText: "Contrato",
                          prefixIcon: Icon(Icons.border_color, 
                          color: colorCurve,),
                          border: new OutlineInputBorder(
                                      borderSide: const BorderSide(color: Color.fromRGBO(0, 94, 184, 9), width: 0.0),
                                      borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                )
                                
                                ),
                              // hintText: "Contrato"
                                ),
                         // validator:  validateMobile,
                          onSaved: (String val) {
                            this._mobile = val;
                          },
                        
                          ),
                              data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                    ),

          SizedBox(height: 20),

         _addImg(),

         SizedBox(height: 20),

        new SizedBox(
          height: 10.0,
        ),
         _submitButtonWidget(),
      ],
    );
  }
  



 GestureDetector  _addImg(){
 return GestureDetector(
        onTap: (){
          showModalBottomSheet(context: context,
                    builder: (BuildContext context) {
                      return SafeArea(
                        child: new Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            new ListTile(
                              leading: new Icon(Icons.camera),
                              title: new Text('Camara'),
                              onTap: () {
                                _onImageButtonPressed(ImageSource.camera, isCamara = true);
                                Navigator.pop(context);
                              },
                            ),
                            new ListTile(
                              leading: new Icon(Icons.image),
                              title: new Text('Galeria'),
                              onTap: () {
                                _onImageButtonPressed(ImageSource.gallery, isCamara = false);
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      );
                    }
                );
        },
        child: new Container(
          color: Colors.white,
          child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children:[ 
                      new Expanded(
                            child: new Theme(
                              child: TextField(
                                enabled: false,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.add_a_photo,
                                  color: prefix0.colorCurve),
                                    border: new OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                  const Radius.circular(10.0),)),
                                  hintText: "Cargar una foto de planilla",
                               ),
                                autofocus: false,
                              ),
                              data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                                  //disabledColor: colorCurve,
                                  canvasColor: colorCurve),
                            ),
                      ),  
                   ],
          ),
        )
    );
}


   void _onImageButtonPressed(ImageSource source, bool esCamara) async {
      try {
        _imageFile = await ImagePicker.pickImage(source: source);
          // vZdar = _imageFile;
          // storage.setItem('VsPreviews', vZdar);

          //  var prueba = storage.getItem('VsPreview');
       
       Navigator.of(context).push(MaterialPageRoute(builder:(context)=>PreviewPage(_imageFile, esCamara)));

      //   Navigator.push(context, MaterialPageRoute(builder: (context) => PreviewPage(_imageFile)));
        // setState(() {
        //  // _onAlertButtonsPressed(context);
        // });
      } catch (e) {
        _pickImageError = e;
      }
  }

   _onAlertButtonsPressed(context) {
    var alertStyle = AlertStyle(
      animationType: AnimationType.fromBottom,
      isCloseButton: false,
      //isOverlayTapDismiss: false,
      //descStyle: TextStyle(fontWeight: FontWeight.bold),
      //animationDuration: Duration(milliseconds: 300),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      // titleStyle: TextStyle(
      //   color: Colors.red,
      // ),
    );


    Alert(
      //style: alertStyle,
      context: context,
      image:  Image.file(_imageFile, height: 300, width: 500),
      title: "Vista previa",
      //desc: "Vista previa.",
      buttons: [
        DialogButton(
          child: Text(
            "Cancelar",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.grey,
        ),
        DialogButton(
          child: Text(
            "Cargar",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
           color: colorCurve,
        )
      ],
    ).show();
  }




    Container _submitButtonWidget() {
    return Container(
      // padding: EdgeInsets.symmetric(
      // vertical: 20, horizontal: 16),
      width: 400,
      height: 60,
      child: RaisedButton(
        //isabledColor: _isButtonDisabled ? colorCurve : Colors.grey,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius: new  BorderRadius.circular(8.0)),
        //padding: EdgeInsets.all(12),
        child: Text(
          "Siguiente",
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
        ),
        color: colorCurve,
        //onPressed: _validateInputs,
        onPressed: _isButtonDisabled ? null : _validateInputs,
      ),
      
    );
  }

    void _validateInputs() {
    

        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => TransactionSteps(1)),
          ModalRoute.withName('/TransactionSteps'),
        );


      if (_formKey.currentState.validate()) {
    //    If all data are correct then save data to out variables
        _formKey.currentState.save();
      } else {
    //    If all data are not valid then start auto validation.
        setState(() {
          _autoValidate = true;
        });
      }
    }

  String validateName(String value) {
    if (value.length < 3)
      return 'Name must be more than 2 charater';
    else
      return null;
  }

  

  void messegerResponse(bool bandera) async {
   
   try {
     await Future.delayed(const Duration(milliseconds: 100));
    setState(() {
       _isButtonDisabled = bandera;
    });
    return null;
   } catch (e) {
      debugPrint('MESSEGER: $e');
   }

 
  }




  Container _selectInfor() {
    return Container(
                padding: EdgeInsets.all(16),
                color: Colors.white,
                
                child: DropDownFormField(
                  
                  titleText: 'Empresa',
                  hintText: 'Seleccione una empresa',
                  value: _myActivity,
                  onSaved: (value) {
                    setState(() {
                      _myActivity = value;
                    });
                  },
                  onChanged: (value) {
                    setState(() {
                      _myActivity = value;
                    });
                  },
                  dataSource: [
                    {
                      "display": "CENL",
                      "value": "Running",
                    },
                    {
                      "display": "CENL",
                      "value": "Climbing",
                    },
                    {
                      "display": "CENL",
                      "value": "Walking",
                    },
                  ],
                  textField: 'display',
                  valueField: 'value',
                ),
              );
  }









  String validateEmail(String value) {
      if (value.length != 10 && value.length != 0 )
      return 'Debe ingresar 10 digitos';
      else
      return null;
  }

Row _helpX(){
 return Row (
    mainAxisAlignment: MainAxisAlignment.center,

    children: [
          
      new Expanded(
        child: new Text ("Solicitud de planilla de servicios básicos actualizada",   style: TextStyle(
                fontFamily: 'Exo2',
                fontWeight: FontWeight.normal, 
                color: Colors.grey, 
                fontSize: 18),
             ) ,
      ),  
      new  Image.asset(
                    "assets/images/hand.png",
                    fit: BoxFit.cover,
                    height: 200,
           ),    
    ],
);
  
}


}