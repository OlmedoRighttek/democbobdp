import 'package:flutter/material.dart';
import 'package:myapp/model/ExapmleNames.dart';
import 'package:myapp/ui/transaction_steps.dart';
import 'package:myapp/utils/ExampleNameItem.dart';
import 'package:myapp/utils/utils.dart';
import 'package:myapp/widgets/widgets.dart';
import 'package:myapp/utils/responsive_screen.dart';

void main() => runApp(new CuentaCredito());

class CuentaCredito extends StatefulWidget {
  CuentaCredito({Key key}) : super(key: key);

  @override
  _PageCuentaCreditoState createState() => _PageCuentaCreditoState();
}

class _PageCuentaCreditoState extends State<CuentaCredito> with SingleTickerProviderStateMixin {
//  _formKey and _autoValidate

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Screen size;

    AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 300),
      value: 1.0,
      vsync: this,
    );
  }


    bool status = false;

  bool get _status {
    final AnimationStatus status = _controller.status;
    return status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
  }

  @override
  Widget build(BuildContext context) {
        size = Screen(MediaQuery.of(context).size);

   return Scaffold(
       backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Column(
          children: <Widget>[
                      SizedBox(height: 60),
                    Row(
                        children: [ 
                        new  IconButton(
                                icon: Icon(Icons.arrow_back,color: colorCurve),
                                onPressed: (navegarPage) ), 
                                 new Expanded(
                                                                                                  child: new GradientText ("Seleccione la cuenta", 
                                                                                                          gradient: LinearGradient(colors: [
                                                                                                          Color.fromRGBO(0, 94, 184, 1.0),  // fromRGBO(0, 94, 184), 
                                                                                                          Color.fromRGBO(0, 42, 102, 1.0)
                                                                                                        ]),
                                                                                                                style: TextStyle(
                                                                                                              fontFamily: 'Exo2', fontSize: 18, fontWeight: FontWeight.bold)
                                                                                                      ) ,
                                                                                                )
                                                                                      ],
                                                                                      ),
                                                                                      _helpX(),
                                                                              Expanded(
                                                                                child: _buildExampleItemsWidget(_status),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        );
                                                       
                                 
                                   }
                                 
                                    _buildExampleItemsWidget(bool status) {
                                                                      if (status) {
                                                                        return ListView.builder(
                                                                          itemBuilder: (BuildContext context, int index) => ExampleNameItem(
                                                                            exampleNames: names[index],
                                                                          ),
                                                                          itemCount: names.length,
                                                                          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 28.0),
                                                                        );
                                                                      } else {
                                                                        return GridView.builder(
                                                                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                                                            crossAxisCount: 2,
                                                                            childAspectRatio: 3.0,
                                                                          ),
                                                                          itemBuilder: (BuildContext context, int index) => ExampleNameItem(
                                                                            exampleNames: names[index],
                                                                          ),
                                                                          itemCount: names.length,
                                                                          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                                                                        );
                                                                      }
                                                                    }
                                 
                                   Padding _helpX(){
                                  return Padding (
                                     padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 28.0),
                                             child: new Row(
                                             mainAxisAlignment: MainAxisAlignment.center,
                                                 children: [  
                                                   new Expanded(
                                                     child: new Text ("Escoja la cuenta a la que desea que se le descuente el credito",   style: TextStyle(
                                                             fontFamily: 'Exo2',
                                                             fontWeight: FontWeight.normal, 
                                                             color: Colors.grey, 
                                                             fontSize: 18),
                                                         ) ,
                                                   ),  
                                                   new  Image.asset(
                                                                 "assets/images/hand.png",
                                                                 fit: BoxFit.cover,
                                                                 height: 200,
                                                       ),    
                                                 ],
                                             ),
                                       );
                                 }
                                 
                                   navegarPage() {
                                             Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TransactionSteps(4)));

                                        //   Navigator.push(context, MaterialPageRoute(builder: (context) => TransactionSteps(4)));
                               }


}

final List<ExampleNames> names = <ExampleNames>[
  ExampleNames("Cuenta de ahorros #10XXXXXX55"),
  ExampleNames("Cuenta de ahorros #13XXXXXX57"),
  ExampleNames("Cuenta de ahorros #11XXXXXX65"),
  // ExampleNames("Cuenta de ahorros #10XXXXXX55"),
  // ExampleNames("Cuenta de ahorros #10XXXXXX55"),
  // ExampleNames("Cuenta de ahorros #10XXXXXX55"),
  // ExampleNames("Cuenta de ahorros #10XXXXXX55"),
  // ExampleNames("Cuenta de ahorros #10XXXXXX55"),
];