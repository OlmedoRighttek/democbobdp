import 'package:flutter/material.dart';
import 'package:myapp/ui/start_request.dart';
import 'package:myapp/utils/utils.dart';
import 'package:myapp/widgets/fitnessAppHomeScreen.dart';
import 'package:myapp/widgets/widgets.dart';
import 'package:myapp/utils/responsive_screen.dart';
import 'package:flutter/gestures.dart';
import 'page_signup.dart';
import 'package:progress_dialog/progress_dialog.dart';
ProgressDialog pr;


void main() => runApp(new LoginPage());

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
//  _formKey and _autoValidate

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = true;
  bool isAceptarTerminos = true;
  bool flag = true;
  bool _isButtonDisabled = false; //true;
  String _email;
  String _mobile;
  Screen size;
  String userIntermatico = "Usuario de Intermático";
  String imageDescription = "";
  String descriptionIntro = "Para empezar con tu solicitud,  inicia sesión con tu usuario y contraseña del Banco del Pacífico";
  String imageUser = "assets/icons/Facial.png";
  String titleText = "Inicio de seción";
  bool isPassword = false;
  @override
  Widget build(BuildContext context) {

    pr = new ProgressDialog(context, type: ProgressDialogType.Download);

    pr.style(
      message: 'Cargando...',
      borderRadius: 20.0,
      backgroundColor: Colors.black12,
      elevation: 12.0,
      insetAnimCurve: Curves.easeInOut,
      progress: 0.0,
      maxProgress: 0.0,
      progressTextStyle: TextStyle(
          color: Colors.white, fontSize: 0.0, fontWeight: FontWeight.w400),
      messageTextStyle: TextStyle(
          color: Colors.white, fontSize: 19.0, fontWeight: FontWeight.w600),
    );


    size = Screen(MediaQuery.of(context).size);

   return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Stack(children: <Widget>[
      
    SingleChildScrollView(
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20), vertical: size.getWidthPx(20)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                   Row(
                    //  mainAxisAlignment: MainAxisAlignment.center => Center Row contents horizontally,
                    //  crossAxisAlignment: CrossAxisAlignment.center => Center Row contents vertically,
                  
                    //mainAxisAlignment: isPassword == true ? MainAxisAlignment.start : MainAxisAlignment.center, 
                    children: <Widget>[                  
                     _buttonReturn(),
                    // isPassword == true ?  _buttonReturn() : _buttonReturn0(),
                      SizedBox(width: size.getWidthPx(20)),
                      _signUpGradientText(),
                    ],
                  ),

                  SizedBox(height: 40),
                    Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: FormUI(),
                        ),
                   ]),
          ),
        ),
      )
    ]));
 

  }

// Here is our Form UI
Widget FormUI() {
    return new Column(
      children: <Widget>[
       _helpX(),
      SizedBox(height: 50),

        new Theme(
                      child: TextFormField(
                          //  maxLength: 10,
                          // keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            // errorText: "ssss",
                            // helperText: "ZIP-Code of shipping destination",
                            labelText: userIntermatico,
                          prefixIcon: Icon(Icons.person, 
                          color: colorCurve,),
                          border: new OutlineInputBorder(
                                      borderSide: const BorderSide(color: Color.fromRGBO(0, 94, 184, 9), width: 0.0),
                                      borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                )
                                
                                ),
                              // hintText: "Contrato"
                                ),
                          validator:  validateMobile,
                          onSaved: (String val) {
                            _mobile = val;
                          },
                        
                          ),
                              data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                    ),
          SizedBox(height: 30),
        new SizedBox(
          height: 10.0,
        ),
         _submitButtonWidget(),
          SizedBox(height: 40),
          _textHelp(),
      ],
    );
  }
  

  GradientText _signUpGradientText() {
    return GradientText(titleText,
        gradient: LinearGradient(colors: [
          Color.fromRGBO(0, 94, 184, 1.0),  // fromRGBO(0, 94, 184), 
          Color.fromRGBO(0, 42, 102, 1.0)
        ]),
        style: TextStyle(fontFamily: 'Exo2',fontSize: 26, fontWeight: FontWeight.bold));
  }

 RichText _textHelp() {
    return RichText(
      text: TextSpan(
          children: [
            TextSpan(
              style: TextStyle(color: Color.fromRGBO(0, 94, 184, 0.8),),
              text: '¿Necesitas ayuda?',
              recognizer: TapGestureRecognizer()
                ..onTap = () =>  Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpPage())),
            )
          ],
          style: TextStyle(color: Colors.black87, fontSize: 20.0, fontFamily: 'Exo2')),
    );
  }

    Container _submitButtonWidget() {
    return Container(
      // padding: EdgeInsets.symmetric(
      // vertical: 20, horizontal: 16),
      width: 400,
      height: 60,
      child: RaisedButton(
        //isabledColor: _isButtonDisabled ? colorCurve : Colors.grey,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius: new  BorderRadius.circular(8.0)),
        //padding: EdgeInsets.all(12),
        child: Text(
          "Siguiente",
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
        ),
        color: colorCurve,
        //onPressed: _validateInputs,
        onPressed: _isButtonDisabled ? null : _validateInputs,
      ),
      
    );
  }

    void _validateInputs() async {
      // pr.show();
    if(this.isPassword){
      //  Future.delayed(Duration(seconds: 1)).then((value) {
      //                 pr.hide().whenComplete(() {
      //                   print(pr.isShowing());
      //                 });
      //               });
        Navigator.push(context, MaterialPageRoute(builder: (context) => FitnessAppHomeScreen(this.isAceptarTerminos)));
       // Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));

       
       
    }else{

     setState(() {
        this.userIntermatico = "Contraseña intermático";
        this.imageDescription = "Trebol";
        this.descriptionIntro = "Antes de ingresar su contraseña por favor verifique que la frase e imagen de seguridad sean las seleccionadas por usted. Si no son las correctas, No ingrese y comuníquese inmediatamento con la Banca Telefónica";
        this.imageUser = "assets/icons/trebol.png";
        this.titleText = "Ingreso de contraseña";
        this.isPassword = true;
          Future.delayed(Duration(seconds: 1)).then((value) {
                      pr.hide().whenComplete(() {
                        print(pr.isShowing());
                      });
                    });

    });
    }
        
   
    
    //   if (_formKey.currentState.validate()) {
    // //    If all data are correct then save data to out variables
    //     _formKey.currentState.save();
    //   } else {
    // //    If all data are not valid then start auto validation.
    //     setState(() {
    //       _autoValidate = true;
    //     });
    //   }
    }



  String validateMobile(String cedula) {
       try {
                  if (cedula.length != 10 && cedula.length != 0 ){
                      messegerResponse(true);
                  return 'Debe ingresar 10 digitos numericos';
                  }
                  else{
                    return null;
                  }
        } catch (e) 
        {
       // debugPrint('CEDULA: $e');
        }
  }


  void messegerResponse(bool bandera) async {
   
   try {
     await Future.delayed(const Duration(milliseconds: 100));
    setState(() {
       _isButtonDisabled = bandera;
    });
    return null;
   } catch (e) {
      debugPrint('MESSEGER: $e');
   }

 
  }



Column _helpX(){
 return Column (
     mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Image.asset(
            imageUser,
            width: 120,
            height: 120,
            ),
          new Text(imageDescription),
            // assets/icons/image.png

          //_passwordIconWidget(),
          SizedBox(height: 20),
          Text(
            descriptionIntro,
            style: TextStyle(
                fontFamily: 'Exo2',
                fontSize: 16.0,
                fontStyle: FontStyle.normal),
          ),
        ],
   );
}

  Container _buttonReturn(){
    return Container(
          child: IconButton (
                              icon: Icon(Icons.arrow_back,color: colorCurve,),
                              onPressed: () {
                                if(isPassword){
                                _returnLogin();
                                }else{
                                Navigator.pop(context, false);
                                // Navigator.push(context, MaterialPageRoute(builder: (context) => CourseInfoScreen()));
                                }
                              },
                       ),
    );
  }
  
void _returnLogin(){
        setState(() {
              this.userIntermatico = "Usuario intermático";
              this.imageDescription = "";
              this.descriptionIntro = "Para empezar con tu solicitud,  inicia sesión con tu usuario y contraseña del Banco del Pacífico";
              this.imageUser = "assets/icons/Facial.png";
              this.titleText = "Inicio de seción";
              this.isPassword = false;
          });
}

//  Container _buttonReturn0(){
//     return Container(
       
//     );
//   }
 
}