import 'package:flutter/material.dart';
import 'package:myapp/utils/utils.dart' as prefix0;
// import 'package:flutter_ui_collections/ui/page_login.dart';
// import 'package:flutter_ui_collections/utils/Constants.dart';
//import 'package:flutter_ui_collections/widgets/dots_indicator.dart';
//import 'package:flutter_ui_collections/utils/utils.dart';
import 'package:myapp/widgets/dots_indicator.dart';
import 'package:myapp/utils/Constants.dart';
import 'package:myapp/utils/utils.dart';

import '../LocalBindings.dart';
import 'home_viwe.dart';
import 'intro_page.dart';

class OnBoardingPage extends StatefulWidget {
  OnBoardingPage({Key key}) : super(key: key);

  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  final _controller = PageController();
  bool leadingVisibility = false;
  Screen  size;

  final List<Widget> _pages = [
    IntroPage("assets/onboard_1.png","Acceder a Microcréditos", "A través de Banca de Oportunidades BdP Banco del Pacífico te abre las puertas a Para que puedas emprender y hacer crecer tu negocio, de una manera sencilla, de una manera digital"),
    IntroPage("assets/onboard_2.png","Sin garantes", "Si eres comerciante autónomo, puedes obtener tu crédito con las siguientes condiciones:\n \n Monto mínimo 300, máximo 500. \n Tasa de interés del 15%.. \n Desde 3 meses hasta 24 meses de financiamiento.."),
    IntroPage("assets/onboard_3.png","Imnovador", "Descubre una nueva forma de hacer las cosas, lo instantáneo es la nueva tendencia, el mundo cambio y tú Banco Banco también!!!"),
  ];
  int currentPageIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    bool isLastPage = currentPageIndex == _pages.length - 1;
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: SafeArea(
          top: false,
          bottom: false,
          child: Stack(
            children: <Widget>[
              pageViewFillWidget(),
              appBarWithButton(isLastPage, context),
              bottomDotsWidget()
            ],
          ),
        ));
  }

  Positioned bottomDotsWidget() {
    return Positioned(
        bottom: size.getWidthPx(20),
        left: 0.0,
        right: 0.0,
        child: DotsIndicator(
          controller: _controller,
          itemCount: _pages.length,
          color: colorCurve,
          onPageSelected: (int page) {
            _controller.animateToPage(
              page,
              duration: const Duration(milliseconds: 300),
              curve: Curves.ease,
            );
          },
        ));
  }

  Positioned appBarWithButton(bool isLastPage, BuildContext context) {
    return Positioned(
     bottom: size.getWidthPx(-5),
      left: 0.0,
      right: 0.0,
      
      child: new SafeArea(
        child: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          primary: false,
          centerTitle: true,
          leading: Visibility(
              visible: leadingVisibility,
              child: IconButton(
                icon: const Icon(Icons.arrow_back),
                color: prefix0.colorCurve,
                onPressed: () {
                  _controller.animateToPage(currentPageIndex - 1,
                      duration: Duration(milliseconds: 300),
                      curve: Curves.easeOut);
                },
              )),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: size.getWidthPx(0), right:  size.getWidthPx(12), bottom: size.getWidthPx(12)),
              
      child: 
      Container(
    height: 50.0,
    child: GestureDetector(
        onTap: () {
          _navegation_intro(isLastPage);          
        },
        child: Container(
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.white,
                    style: BorderStyle.solid,
                    width: 1.0,
                ),
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(30.0),
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                    Center(
                        child: Text(
                           isLastPage ? 'HECHO' : 'SIGUIENTE',
                            style: TextStyle(
                                color: prefix0.colorCurve,
                                fontFamily: 'Montserrat',
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1,
                            ),
                        ),
                    )
                ],
            ),
        ),
    ),
)

            )
          ],
        ),
      ),
    );
  }

void _navegation_intro(isLastPage){
    if(isLastPage){
     LocalStorage.sharedInstance.writeValue(key:Constants.isOnBoard,value: "1");
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeView()));
    }else{
     _controller.animateToPage(currentPageIndex + 1,
                      duration: Duration(milliseconds: 300),
                      curve: Curves.easeIn);
    }
}

  Positioned pageViewFillWidget() {
    return Positioned.fill(
        child: PageView.builder(
          controller: _controller,
          itemCount: _pages.length,
          itemBuilder: (BuildContext context, int index) {
            return _pages[index % _pages.length];
          },
          onPageChanged: (int p) {
            setState(() {
              currentPageIndex = p;
              if (currentPageIndex == 0) {
                leadingVisibility = false;
              } else {
                leadingVisibility = true;
              }
            });
          },
        ));
  }
}