import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myapp/utils/colors.dart';
import 'package:myapp/utils/hotelAppTheme.dart';
import 'package:myapp/model/selectListDat.dart';
import 'package:myapp/model/selectListView.dart';
import 'package:localstorage/localstorage.dart';

class HomeView extends StatefulWidget {
  final AnimationController animationController;

  const HomeView({Key key, this.animationController}) : super(key: key);
  @override
  _HomeViewScreenState createState() => _HomeViewScreenState();
}



class _HomeViewScreenState extends State<HomeView> with TickerProviderStateMixin {
  
  
  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        //title: new Text('¿Estás seguro?'),
        content: new Text('¿Estás seguro que quieres salir de la aplicación?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => SystemNavigator.pop() , // Navigator.of(context).pop(true),exit(0)
            child: new Text('Si'),
          ),
        ],
      ),
    )) ?? false;
  }
  
  
    final LocalStorage storage = new LocalStorage('todo_app');
  AnimationController animationController;
  var hotelList = HotelListData.hotelList;
  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now().add(Duration(days: 5));

  @override
  void initState() {
    animationController = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);
    super.initState();
  }

  Future<bool> getData() async {
    await Future.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
  return new WillPopScope(
      onWillPop: _onWillPop,
      child:Theme(
      data: HotelAppTheme.buildLightTheme(),
      child: Container(
        child: Scaffold(
          body: Stack(
            children: <Widget>[
              InkWell(
                splashColor: Colors.transparent,
                focusColor: Colors.transparent,
                highlightColor: Colors.transparent,
                hoverColor: Colors.transparent,
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: Column(
                  children: <Widget>[
                    //getAppBarUI(),
                  AppBar(
                    automaticallyImplyLeading: false,
                    centerTitle: true,
                    title: Text("Banco del Pacífico",
                        style:
                        TextStyle(fontSize: 22,fontFamily: "Exo2", color: backgroundColor)),
                    backgroundColor: colorCurve,
                  ),
                    //  Padding(
                    //         padding: const EdgeInsets.only(
                    //             top: 20.0, left: 18, right: 16),
                    //         child: Text(
                    //           "BIENVENIDO/A",
                    //           textAlign: TextAlign.start,
                    //           style: TextStyle(
                    //             fontWeight: FontWeight.w600,
                    //             fontSize: 20,
                    //             letterSpacing: 0.27,
                    //             color: Colors.grey,
                    //           ),
                    //         ),
                    //       ),
                              Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0, left: 18, right: 16),
                            child: Text(
                              "¿Qué desea solicitar?",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 25,
                                letterSpacing: 0.27,
                                color: Colors.black,
                              ),
                            ),
                          ),
                    Expanded(
                      child: Container(
                          // color: HotelAppTheme.buildLightTheme().backgroundColor,
                          child: ListView.builder(
                            itemCount: hotelList.length,
                            padding: EdgeInsets.only(top: 0),
                            scrollDirection: Axis.vertical,
                            itemBuilder: (context, index) {
                              var count = hotelList.length > 10 ? 10 : hotelList.length;
                              var animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                                  parent: animationController, curve: Interval((1 / count) * index, 1.0, curve: Curves.fastOutSlowIn)));
                              animationController.forward();
                              return HotelListView(
                                callback: () {
                                   _save(hotelList[index]);
                                   // moveTo();
                                },
                                hotelData: hotelList[index],
                                animation: animation,
                                animationController: animationController,
                              );
                            },
                          ),
                        ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    )
  );

    
    // return Theme(
    //   data: HotelAppTheme.buildLightTheme(),
    //   child: Container(
    //     child: Scaffold(
    //       body: Stack(
    //         children: <Widget>[
    //           InkWell(
    //             splashColor: Colors.transparent,
    //             focusColor: Colors.transparent,
    //             highlightColor: Colors.transparent,
    //             hoverColor: Colors.transparent,
    //             onTap: () {
    //               FocusScope.of(context).requestFocus(FocusNode());
    //             },
    //             child: Column(
    //               children: <Widget>[
    //                 //getAppBarUI(),
    //               AppBar(
    //                 automaticallyImplyLeading: false,
    //                 centerTitle: true,
    //                 title: Text("Banco del Pacífico",
    //                     style:
    //                     TextStyle(fontSize: 22,fontFamily: "Exo2", color: backgroundColor)),
    //                 backgroundColor: colorCurve,
    //               ),
    //                 //  Padding(
    //                 //         padding: const EdgeInsets.only(
    //                 //             top: 20.0, left: 18, right: 16),
    //                 //         child: Text(
    //                 //           "BIENVENIDO/A",
    //                 //           textAlign: TextAlign.start,
    //                 //           style: TextStyle(
    //                 //             fontWeight: FontWeight.w600,
    //                 //             fontSize: 20,
    //                 //             letterSpacing: 0.27,
    //                 //             color: Colors.grey,
    //                 //           ),
    //                 //         ),
    //                 //       ),
    //                           Padding(
    //                         padding: const EdgeInsets.only(
    //                             top: 10.0, left: 18, right: 16),
    //                         child: Text(
    //                           "¿Qué desea solicitar?",
    //                           textAlign: TextAlign.start,
    //                           style: TextStyle(
    //                             fontWeight: FontWeight.w600,
    //                             fontSize: 25,
    //                             letterSpacing: 0.27,
    //                             color: Colors.black,
    //                           ),
    //                         ),
    //                       ),
    //                 Expanded(
    //                   child: Container(
    //                       color: HotelAppTheme.buildLightTheme().backgroundColor,
    //                       child: ListView.builder(
    //                         itemCount: hotelList.length,
    //                         padding: EdgeInsets.only(top: 0),
    //                         scrollDirection: Axis.vertical,
    //                         itemBuilder: (context, index) {
    //                           var count = hotelList.length > 10 ? 10 : hotelList.length;
    //                           var animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
    //                               parent: animationController, curve: Interval((1 / count) * index, 1.0, curve: Curves.fastOutSlowIn)));
    //                           animationController.forward();
    //                           return HotelListView(
    //                             callback: () {
    //                                _save(hotelList[index]);
    //                                // moveTo();
    //                             },
    //                             hotelData: hotelList[index],
    //                             animation: animation,
    //                             animationController: animationController,
    //                           );
    //                         },
    //                       ),
    //                     ),
    //                 )
    //               ],
    //             ),
    //           ),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  
  
  
  }




  //   void moveTo() {
  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(
  //       builder: (context) => CourseInfoScreen(),
  //     ),
  //   );
  // }



  void _save(HotelListData hotelList) {
    //"hotelList.imagePath"
    storage.setItem('Img', 'assets/design_course/webInterFace.png');

  }

 

  _clearStorage() async {
    await storage.clear();

  }
    


  Widget getHotelViewList() {
    List<Widget> hotelListViews = List<Widget>();
    for (var i = 0; i < hotelList.length; i++) {
      var count = hotelList.length;
      var animation = Tween(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
          parent: animationController,
          curve: Interval((1 / count) * i, 1.0, curve: Curves.fastOutSlowIn),
        ),
      );
      hotelListViews.add(
        HotelListView(
          callback: () {},
          hotelData: hotelList[i],
          animation: animation,
          animationController: animationController,
        ),
      );
    }
    animationController.forward();
    return Column(
      children: hotelListViews,
    );
  }

 

  Widget getAppBarUI() {
    return Container(
      decoration: BoxDecoration(
        color: colorCurve,
        boxShadow: <BoxShadow>[
          BoxShadow(color: Colors.grey.withOpacity(0.2), offset: Offset(0, 2), blurRadius: 8.0),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top, left: 8, right: 8),
        child: Row(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              //width: AppBar().preferredSize.height,
              height: AppBar().preferredSize.height,
              child: Material(
                color: Colors.transparent,
                // child: InkWell(
                //   borderRadius: BorderRadius.all(
                //     Radius.circular(32.0),
                //   ),
                //   onTap: () {
                //     Navigator.pop(context);
                //   },
                //   // child: Padding(
                //   //   padding: const EdgeInsets.all(8.0),
                    
                //   //   child: Icon(Icons.arrow_back_ios,   color: Colors.white,),
                //   // ),
                // ),
              ),
            ),
            Expanded(
                child: Text(
                  "Banco del Pacífico",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                    color: Colors.white
                  ),
                ),
            ),
            // Container(
            //   width: AppBar().preferredSize.height + 40,
            //   height: AppBar().preferredSize.height,
            //   child: Row(
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     mainAxisAlignment: MainAxisAlignment.end,
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}


