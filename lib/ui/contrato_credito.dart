import 'package:flutter/material.dart';
import 'package:myapp/utils/utils.dart';
import 'package:myapp/utils/utils.dart' as prefix0;
import 'package:myapp/widgets/widgets.dart';
import 'package:myapp/utils/responsive_screen.dart';

import 'otp_page.dart';

void main() => runApp(new ContratoCredito());

class ContratoCredito extends StatefulWidget {
  ContratoCredito({Key key}) : super(key: key);

  @override
  _PageContratoCreditoState createState() => _PageContratoCreditoState();
}

class _PageContratoCreditoState extends State<ContratoCredito> {
//  _formKey and _autoValidate

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = true;
  bool flag = true;
  bool _isButtonDisabled = false; //true;
  String _email;
  String _mobile;
  Screen size;

  @override
  Widget build(BuildContext context) {
        size = Screen(MediaQuery.of(context).size);

   return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Stack(children: <Widget>[
      
    SingleChildScrollView(
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20), vertical: size.getWidthPx(20)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                      children: [ 
                       new  IconButton(
                              icon: Icon(Icons.arrow_back,color: colorCurve),
                              onPressed: () => Navigator.pop(context, false),
                            ), 
                          new Expanded(
                              child: new GradientText ("Código de cofirmación", 
                                      gradient: LinearGradient(colors: [
                                      Color.fromRGBO(0, 94, 184, 1.0),  // fromRGBO(0, 94, 184), 
                                      Color.fromRGBO(0, 42, 102, 1.0)
                                    ]),
                                            style: TextStyle(
                                           fontFamily: 'Exo2', fontSize: 18, fontWeight: FontWeight.bold)
                                  ) ,
                            )
                   ],
                  ),
                    Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: FormUI(),
                        ),
                   ]),
          ),
        ),
      )
    ]));
 

  }

// Here is our Form UI
Widget FormUI() {
    return new Column(
      children: <Widget>[

      //   new TextFormField(
      //     decoration: const InputDecoration(labelText: 'Name'),
      //     keyboardType: TextInputType.text,
      //     validator: validateName,
      //     onSaved: (String val) {
      //       _name = val;
      //     },
      //   ),
       _helpX(),

      


      SizedBox(height: 20),

        new Theme(
                      child: TextFormField(
                          enabled: false,
                           maxLength: 10,
                          controller: TextEditingController(text: "0xxxxx6510"),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            
                            // errorText: "ssss",
                            // helperText: "ZIP-Code of shipping destination",
                            labelText: "Número de celular",
                          prefixIcon: Icon(Icons.message, 
                          color: colorCurve,),
                                border: new OutlineInputBorder(
                                            borderSide: const BorderSide(color: Color.fromRGBO(0, 94, 184, 9), width: 0.0),
                                            borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                      )
                                ),
                              // hintText: "Contrato"
                                ),
                          //validator:  validateMobile,
                          onSaved: (String val) {
                            _mobile = val;
                          },
                        
                          ),
                              data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                    ),

          SizedBox(height: 20),

   
            new Theme(
                      child: TextFormField(
                            autofocus: false,
                            autocorrect: false,
                            enabled: false,
                            controller: TextEditingController(text: "oxxxxa@pacifico.fim.ec"),
                            textCapitalization: TextCapitalization.characters,
                            decoration: InputDecoration(
                                          fillColor: colorCurve,
                                          focusColor: colorCurve,
                                          hoverColor: colorCurve,
                                            labelText: "Correo electrónico",
                                            prefixIcon: Icon(Icons.email, 
                                            color: prefix0.colorCurve),
                                            border: new OutlineInputBorder(
                                                        borderRadius: const BorderRadius.all(
                                                        const Radius.circular(10.0),
                                                  )
                                                  
                                                  ),
                                                // hintText: "Contrato"
                                                  ),
                            keyboardType: TextInputType.emailAddress,
                            validator:  validateEmail,
                            onSaved: (String val) {
                              _email = val;
                            },
                        
                          ),
                              data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                    ),

        
          SizedBox(height: 20),

    
        new SizedBox(
          height: 10.0,
        ),
         _submitButtonWidget(),
      ],
    );
  }
  


    Container _submitButtonWidget() {
    return Container(
      // padding: EdgeInsets.symmetric(
      // vertical: 20, horizontal: 16),
      width: 400,
      height: 60,
      child: RaisedButton(
        //isabledColor: _isButtonDisabled ? colorCurve : Colors.grey,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius: new  BorderRadius.circular(8.0)),
        //padding: EdgeInsets.all(12),
        child: Text(
          "Enviar código",
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
        ),
        color: colorCurve,
        //onPressed: _validateInputs,
        onPressed: _isButtonDisabled ? null : _validateInputs,
      ),
    );
  }

    void _validateInputs() {
        Navigator.push(context, MaterialPageRoute(builder: (context) => PinCodeVerificationScreen("Cel: 0969796510, Email: za-olmedo@hotmail.com")));

      if (_formKey.currentState.validate()) {
    //    If all data are correct then save data to out variables
        _formKey.currentState.save();
      } else {
    //    If all data are not valid then start auto validation.
        setState(() {
          _autoValidate = true;
        });
      }
    }

  String validateName(String value) {
    if (value.length < 3)
      return 'Name must be more than 2 charater';
    else
      return null;
  }


  void messegerResponse(bool bandera) async {
        try {
          await Future.delayed(const Duration(milliseconds: 100));
          setState(() {
            _isButtonDisabled = bandera;
          });
          return null;
        } catch (e) {
            debugPrint('MESSEGER: $e');
        }
  }


  String validateEmail(String value) {
      if (value.length != 10 && value.length != 0 )
      return 'Debe ingresar 10 digitos';
      else
      return null;
  }

    Row _helpX(){
          return Row (
              mainAxisAlignment: MainAxisAlignment.center,
              children: [  
                new Expanded(
                  child: new Text ("Enviaremos un codigo de confirmación a su información de contacto registrada",   style: TextStyle(
                          fontFamily: 'Exo2',
                          fontWeight: FontWeight.normal, 
                          color: Colors.grey, 
                          fontSize: 18),
                      ) ,
                ),  
                new  Image.asset(
                              "assets/images/hand.png",
                              fit: BoxFit.cover,
                              height: 200,
                    ),    
              ],
          );
    }
}