
import 'package:flutter/material.dart';
import 'package:myapp/ui/transaction_steps.dart';
import 'package:myapp/utils/colors.dart';
import 'package:myapp/utils/dropdown_formfield.dart';
import 'package:myapp/utils/responsive_screen.dart';
import 'package:myapp/widgets/gradient_text.dart';

class SimuladorCredito extends StatefulWidget {
  @override
  _PageSimuladorCreditoState createState() => _PageSimuladorCreditoState();
}

class _PageSimuladorCreditoState extends State<SimuladorCredito> {
  Screen size;
  String _myActivity;
  String _myActivity2;
  bool isCalculator = false; 
  String dollars = "300";


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
      size = Screen(MediaQuery.of(context).size);

   return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Stack(children: <Widget>[
      
    SingleChildScrollView(
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20), vertical: size.getWidthPx(20)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                      children: [ 
                       new  IconButton(
                              icon: Icon(Icons.arrow_back,color: colorCurve),
                              onPressed: () => Navigator.pop(context, false),
                            ), 
                          new Expanded(
                              child: new GradientText ("Simulador de crédito", 
                                      gradient: LinearGradient(colors: [
                                      Color.fromRGBO(0, 94, 184, 1.0),  // fromRGBO(0, 94, 184), 
                                      Color.fromRGBO(0, 42, 102, 1.0)
                                    ]),
                                            style: TextStyle(
                                           fontFamily: 'Exo2', fontSize: 18, fontWeight: FontWeight.bold)
                                  ) ,
                            )
                   ],
                  ),
                    Form(
                          child: FormUI(),
                        ),
                   ]),
          ),
        ),
      )
    ]));
  }

Widget FormUI() {
    return new Column(
      children: <Widget>[
                  if (!isCalculator)
                  _helpX() ,
      //isCalculator == false ? _helpX() : null,
       Container(child: new Text(
         "Valor máximo a prestar \$ 1500",
         style: TextStyle(
                          fontFamily: 'Exo2',
                          fontWeight: FontWeight.bold,  
                          color: Colors.grey, 
                          fontSize: 18
                          ),
         )),
          SizedBox(height: 10),
            _selectInfor(),
          SizedBox(height: 20),
            _selectMes(),
          Container(child: new Text(
                "Se aplicara un seguro del degravamen del 0.02%",
                style: TextStyle(
                              fontFamily: 'Exo2',
                              fontWeight: FontWeight.bold, 
                              color: Colors.grey, 
                              fontSize: 18),
           )),

         
            SizedBox(height: 20),

        _submitButtonWidget(),

          SizedBox(height: 30),

          if (isCalculator) 
          _rowCalculator(), 

          SizedBox(height: 30),

          if (isCalculator) 
          _submitButtonWidget2()
          

    // isCalculator == true ?    _rowCalculator(): null,
    //         SizedBox(height: 30),

    //  isCalculator == true ?   _submitButtonWidget2(): null,
      ],
    );
  }
  

  Container _selectInfor() {
    return Container(
                padding: EdgeInsets.all(16),
                color: Colors.white,
                child: DropDownFormField(
                  titleText: 'Monto de credito',
                  hintText: 'Elija el monto a solicitar',
                  value: _myActivity,
                  onSaved: (value) {
                    setState(() {
                      _myActivity = value;
                    });
                  },
                  onChanged: (value) {
                    setState(() {
                      _myActivity = value;
                    });
                  },
                  dataSource: [
                    {
                      "display": "\$ $dollars.",
                      "value": "300",
                    },{
                      "display": "\$ 500",
                      "value": "500",
                    },
                    {
                      "display": "\$ 600",
                      "value": "600",
                    },
                    {
                      "display": "\$ 700",
                      "value": "700",
                    },
                     {
                      "display": "\$ 1100",
                      "value": "1100",
                    },
                     {
                      "display": "\$ 1500",
                      "value": "1500",
                    },
                  ],
                  textField: 'display',
                  valueField: 'value',
                ),
              );
  }

 Container _selectMes() {
    return Container(
                padding: EdgeInsets.all(16),
                color: Colors.white,
                child: DropDownFormField(
                  titleText: 'Meses a pagar',
                  hintText: 'En cuántos meses desea pagarlo',
                  value: _myActivity2,
                  onSaved: (value) {
                    setState(() {
                      _myActivity2 = value;
                    });
                  },
                  onChanged: (value) {
                    setState(() {
                      _myActivity2 = value;
                    });
                  },
                  dataSource: [
                    {
                      "display": "3 Meses",
                      "value": "3",
                    },{
                      "display": "24 Meses",
                      "value": "24",
                    },
                    {
                      "display": "36 Meses",
                      "value": "36",
                    },
                  ],
                  textField: 'display',
                  valueField: 'value',
                ),
              );
  }


 Container _submitButtonWidget() {
    return Container(
      // padding: EdgeInsets.symmetric(
      // vertical: 20, horizontal: 16),
      width: 400,
      height: 60,
      child: RaisedButton(
        //isabledColor: _isButtonDisabled ? colorCurve : Colors.grey,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius: new  BorderRadius.circular(8.0)),
        //padding: EdgeInsets.all(12),
        child: Text(
          isCalculator == false ? "Calcular" :"Volver a Calcular",
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
        ),
        color: colorCurve,
        //onPressed: _validateInputs,
        onPressed: _validateInputs,
      ),
      
    );
  }


Container _submitButtonWidget2() {
    return Container(
      // padding: EdgeInsets.symmetric(
      // vertical: 20, horizontal: 16),
      width: 400,
      height: 60,
      child: RaisedButton(
        //isabledColor: _isButtonDisabled ? colorCurve : Colors.grey,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius: new  BorderRadius.circular(8.0)),
        //padding: EdgeInsets.all(12),
        child: Text(
          "Generar Credito",
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
        ),
        color: colorCurve,
        //onPressed: _validateInputs,
        onPressed: ()    {
         Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TransactionSteps(3)));

        // Navigator.push(context, MaterialPageRoute(builder: (context) => TransactionSteps(3)));
        },
      ),
      
    );
  }



   void _validateInputs() {
        setState(() {
            isCalculator = true;
        });
    }

Column _rowCalculator(){
    return Column (
     mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row (
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[  
                      new Expanded(
                        child: new Text ("Monto maximo a financiar",   
                        textAlign: TextAlign.center,
                        style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.bold, 
                                color: Colors.grey, 
                                fontSize: 18),
                            ) ,
                      ),  
                      new Expanded(
                        child: new Text ("Monto maximo a financiar",   
                        textAlign: TextAlign.center,
                        style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.bold, 
                                color: Colors.grey, 
                                fontSize: 18),
                            ) ,
                      ), 
                      new Expanded(
                        child: new Text ("Monto maximo a financiar",   
                       textAlign: TextAlign.center,
                        style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.bold, 
                                color: Colors.grey, 
                                fontSize: 18),
                            ) ,
                      ), 
                   ],
          ),
          SizedBox(height: 20),
              Row (
          mainAxisAlignment: MainAxisAlignment.center,
          children: [  
                      new Expanded(
                        child: new Text ("\$ 300",
                                textAlign: TextAlign.center,
                            style: TextStyle(
                              
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.normal, 
                                color: Colors.grey, 
                                fontSize: 18),
                            ) ,
                      ), 
                        new Expanded(
                        child: new Text ("3",                                 
                           textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.normal, 
                                color: Colors.grey, 
                                fontSize: 18),
                            ) ,
                      ),
                        new Expanded(
                        child: new Text ("\$ 102.51",   
                          textAlign: TextAlign.center,
                          style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.normal, 
                                color: Colors.grey, 
                                fontSize: 18),
                            ) ,
                      ), 
                   ],
          ),
        ],
   );
}
       


Row _helpX(){
      return Row (
          mainAxisAlignment: MainAxisAlignment.center,
          children: [  
            new Expanded(
              
              child: new Text ("Calcula el monto a solicitar, los meses de plazo y cuotas de pago",  
               style: TextStyle(
                      fontFamily: 'Exo2',
                      fontWeight: FontWeight.normal, 
                      color: Colors.grey, 
                      fontSize: 18),
                  ) ,
            ),  
            new  Image.asset(
                          "assets/images/hand.png",
                          fit: BoxFit.cover,
                          height: 200,
                ),    
          ],
      ); 
   }
}