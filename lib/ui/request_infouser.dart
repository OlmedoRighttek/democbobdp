import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myapp/ui/request_service_basic.dart';
import 'package:myapp/utils/utils.dart';
import 'package:myapp/utils/utils.dart' as prefix0;
import 'package:myapp/widgets/widgets.dart';
import 'package:myapp/utils/responsive_screen.dart';

void main() => runApp(new RequestInfoUser());

class RequestInfoUser extends StatefulWidget {
  RequestInfoUser({Key key}) : super(key: key);

  @override
  _RequestPageInfoUser createState() => _RequestPageInfoUser();
}

class _RequestPageInfoUser extends State<RequestInfoUser> {
//  _formKey and _autoValidate

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = true;
  bool flag = true;
  bool _isButtonDisabled = false; //true;
  String _email;
  String _mobile;
  Screen size;

  @override
  Widget build(BuildContext context) {
        size = Screen(MediaQuery.of(context).size);

   return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Stack(children: <Widget>[
      
    SingleChildScrollView(
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20), vertical: size.getWidthPx(20)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                      children: [ 
                       new  IconButton(
                              icon: Icon(Icons.arrow_back,color: colorCurve),
                              onPressed: () => Navigator.pop(context, false),
                            ), 
                          new Expanded(
                              child: new GradientText ("Ingreso de información del ciente", 
                                      gradient: LinearGradient(colors: [
                                      Color.fromRGBO(0, 94, 184, 1.0),  // fromRGBO(0, 94, 184), 
                                      Color.fromRGBO(0, 42, 102, 1.0)
                                    ]),
                                            style: TextStyle(
                                           fontFamily: 'Exo2', fontSize: 18, fontWeight: FontWeight.bold)
                                  ) ,
                            )
                   ],
                  ),
                    Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: FormUI(),
                        ),
                   ]),
          ),
        ),
      )
    ]));
 

  }

// Here is our Form UI
Widget FormUI() {
    return new Column(
      children: <Widget>[

      //   new TextFormField(
      //     decoration: const InputDecoration(labelText: 'Name'),
      //     keyboardType: TextInputType.text,
      //     validator: validateName,
      //     onSaved: (String val) {
      //       _name = val;
      //     },
      //   ),
       _helpX(),

      SizedBox(height: 20),

        new Theme(
                      child: TextFormField(
                           maxLength: 10,
                          keyboardType: TextInputType.number,
                           inputFormatters: <TextInputFormatter>[
                              WhitelistingTextInputFormatter.digitsOnly
                          ],
                          decoration: InputDecoration(
                            labelText: "Número de cédula",
                          prefixIcon: Icon(Icons.portrait, 
                          color: colorCurve,),
                          border: new OutlineInputBorder(
                                      borderSide: const BorderSide(color: Color.fromRGBO(0, 94, 184, 9), width: 0.0),
                                      borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                )
                                
                                ),
                              // hintText: "Contrato"
                                ),
                          validator:  validateMobile,
                          onSaved: (String val) {
                            _mobile = val;
                          },
                        
                          ),
                              data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                    ),

          SizedBox(height: 20),

          new Theme(
                      child: TextFormField(
                           maxLength: 10,
                            autofocus: false,
                            autocorrect: false,
                            textCapitalization: TextCapitalization.characters,
                            decoration: InputDecoration(
                                          fillColor: colorCurve,
                                          focusColor: colorCurve,
                                          hoverColor: colorCurve,
                                            labelText: "Código de dactilar",
                                            prefixIcon:  Icon(Icons.fingerprint, 
                                            color: prefix0.colorCurve),
                                            
                                            suffixIcon: IconButton(
                                              icon:  Icon(Icons.help_outline), 
                                               onPressed: (){
                                                 _neverSatisfied();

                                               },
                                            ),
                                             
                                            border: new OutlineInputBorder(
                                                        borderRadius: const BorderRadius.all(
                                                        const Radius.circular(10.0),
                                                  )
                                                  
                                                  ),
                                                // hintText: "Contrato"
                                                  ),
                            keyboardType: TextInputType.emailAddress,
                            validator:  validateEmail,
                            onSaved: (String val) {
                              _email = val;
                            },
                        
                          ),
                              data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                    ),

        
          SizedBox(height: 20),

        // new TextFormField(
        //   maxLength: 10,
        //   autofocus: false,
        //   autocorrect: false,
        //   textCapitalization: TextCapitalization.characters,
        //   decoration: InputDecoration(
        //                  fillColor: colorCurve,
        //                  focusColor: colorCurve,
        //                  hoverColor: colorCurve,
        //                   labelText: "Código de dactilar",
        //                   prefixIcon: Icon(Icons.fingerprint, 
        //                   color: prefix0.colorCurve),
        //                   border: new OutlineInputBorder(
        //                               borderRadius: const BorderRadius.all(
        //                               const Radius.circular(10.0),
        //                         )
                                
        //                         ),
        //                       // hintText: "Contrato"
        //                         ),
        //   keyboardType: TextInputType.emailAddress,
        //   validator:  validateEmail,
        //   onSaved: (String val) {
        //     _email = val;
        //   },
        // ),
     
            SizedBox(height: 20),

        new SizedBox(
          height: 10.0,
        ),
         _submitButtonWidget(),
      ],
    );
  }
  


    Container _submitButtonWidget() {
    return Container(
      // padding: EdgeInsets.symmetric(
      // vertical: 20, horizontal: 16),
      width: 400,
      height: 60,
      child: RaisedButton(
        //isabledColor: _isButtonDisabled ? colorCurve : Colors.grey,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius: new  BorderRadius.circular(8.0)),
        //padding: EdgeInsets.all(12),
        child: Text(
          "Siguiente",
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
        ),
        color: colorCurve,
        //onPressed: _validateInputs,
        onPressed: _isButtonDisabled ? null : _validateInputs,
      ),
      
    );
  }

    void _validateInputs() {
        Navigator.push(context, MaterialPageRoute(builder: (context) => RequestInfoServiceBasic()));

      if (_formKey.currentState.validate()) {
    //    If all data are correct then save data to out variables
        _formKey.currentState.save();
      } else {
    //    If all data are not valid then start auto validation.
        setState(() {
          _autoValidate = true;
        });
      }
    }

  String validateName(String value) {
    if (value.length < 3)
      return 'Name must be more than 2 charater';
    else
      return null;
  }

  String validateMobile(String cedula) {
 try {
    if (cedula.length != 10 && cedula.length != 0 ){
        messegerResponse(true);
     return 'Debe ingresar 10 digitos numericos';
    }
    else{
        if (cedula.length == 10) {
            //Obtenemos el digito de la region que sonlos dos primeros digitos
            var digitoRegion = cedula.substring(0, 2);
            //Pregunto si la region existe ecuador se divide en 24 regiones
            if ((int.parse(digitoRegion) >= 1) && (24 >= int.parse(digitoRegion))) {
                //console.log('La cedula pertenece a una region ' + digitoRegion);
                // Extraigo el ultimo digito
                var ultimoDigito = int.parse(cedula.substring(9, 10));
                //Agrupo todos los pares y los sumo
                var pares = int.parse(cedula.substring(1, 2)) + int.parse(cedula.substring(3, 4)) +
                int.parse(cedula.substring(5, 6)) + int.parse(cedula.substring(7, 8));
                //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
                var numero11 = cedula.substring(0, 1);
                var numero1 = int.parse(numero11);
                numero1 = (numero1 * 2);
                if (numero1 > 9) { numero1 = (numero1 - 9); }
                //debo validar que el numero 3 sea menos a 6
                var numero34 = cedula.substring(2, 3);
                var numero3 = int.parse(numero34);


                if (numero3 >= 6) {
                    flag = false;
                    //console.log('el tercer digito no puede ser mayor o igual a 6');
                    //$.notify({
                    //    // options
                    //    message: 'el tercer digito no puede ser mayor o igual a 6'
                    //}, {
                    //        // settings
                    //        type: 'danger'
                    //    });
                }
                numero3 = (numero3 * 2);

                if (numero3 > 9) { numero3 = (numero3 - 9); }
                var numero5 = int.parse(cedula.substring(4, 5));
                numero5 = (numero5 * 2);
                if (numero5 > 9) { numero5 = (numero5 - 9); }
                var numero7 = int.parse(cedula.substring(6, 7));
                numero7 = (numero7 * 2);
                if (numero7 > 9) { numero7 = (numero7 - 9); }
                var numero9 = int.parse(cedula.substring(8, 9));
                numero9 = (numero9 * 2);
                if (numero9 > 9) { numero9 = (numero9 - 9); }
                var impares = numero1 + numero3 + numero5 + numero7 + numero9;
                //Suma total
                var sumatotal = (pares + impares);
                //extraemos el primero digito
                var primerDigitoSuma = (sumatotal.toString()).substring(0, 1);
                //Obtenemos la decena inmediata
                var decena = (int.tryParse(primerDigitoSuma) + 1) * 10;
                //Obtenemos la resta de la decena inmediata - la sumatotal esto nos da el digito validador
                var digitoValidador = decena - sumatotal;
                //Si el digito validador es = a 10 toma el valor de 0
                if (digitoValidador == 10)
                { digitoValidador = 0; }
                //Validamos que el digito validador sea igual al de la cedula
                var X = cedula == "2222222222" ? false : true;
                if ((digitoValidador == ultimoDigito) && (flag) && (X)) {
                     
                      // setState(() => 
                      // _isButtonDisabled = !_isButtonDisabled);
                    messegerResponse(false);
                } else {
                  messegerResponse(true);
                  return 'Cédula Inválida X';
                }
            }
            else {
                   messegerResponse(true);
                  return 'Cédula Inválida X';
            }
        }
        else {
              //return 'Este campo debe tener 10 digitos';
        }
    }
        } catch (e) 
        {
       // debugPrint('CEDULA: $e');

        }
  }


  void messegerResponse(bool bandera) async {
      try {
        await Future.delayed(const Duration(milliseconds: 100));
        setState(() {
          _isButtonDisabled = bandera;
        });
        return null;
      } catch (e) {
          debugPrint('MESSEGER: $e');
      } 
}


  String validateEmail(String value) {
      if (value.length != 10 && value.length != 0 )
      return 'Debe ingresar 10 digitos';
      else
      return null;
  }

Row _helpX(){
      return Row (
          mainAxisAlignment: MainAxisAlignment.center,
          children: [  
            new Expanded(
              child: new Text ("Digita el número de cédula y el código que se encuentra arriba de la huella dactilar",   style: TextStyle(
                      fontFamily: 'Exo2',
                      fontWeight: FontWeight.normal, 
                      color: Colors.grey, 
                      fontSize: 18),
                  ) ,
            ),  
            new  Image.asset(
                          "assets/images/hand.png",
                          fit: BoxFit.cover,
                          height: 200,
                ),    
          ],
      ); 
}



Future<void> _neverSatisfied() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Código de huella dactilar" ,textAlign: TextAlign.center  ,),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Image.asset('assets/images/ic_help_cedula.png', height: 300, ),
              Text('\n El código de huella dactilar se encuentra en la parte reversa de su cédula de ciudadanía'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Entendido'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
         
        ],
      );
    },
  );
}


}