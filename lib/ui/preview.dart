import 'dart:io';
import 'package:flutter/material.dart';
import 'package:myapp/utils/colors.dart';
import 'package:myapp/utils/responsive_screen.dart';


class PreviewPage extends StatefulWidget {
 // PreviewPage(File imageGenerica);
 File imageGenerica;
 bool esCamara = false;
PreviewPage(this.imageGenerica, this.esCamara);


  @override
  State<StatefulWidget> createState() {
    return _PreviewPageState(this.imageGenerica, this.esCamara);
  }

 // _PreviewPageState createState() => _PreviewPageState(this.imageGenerica);
}

class _PreviewPageState extends State<PreviewPage> {
     bool esCamara = false;
     Screen size;
  
 // final LocalStorage storage = new LocalStorage('todo_app');
    File imageGenerica;

  _PreviewPageState(this.imageGenerica, this.esCamara);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
        appBar: AppBar(
           actions: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.save,
                    color: Colors.white,
                  ),
                 onPressed: () => Navigator.pop(context, false),
                )
              ],
          centerTitle: true,
          title: Text("Vista previa",
              style:
              TextStyle(fontFamily: "Exo2", color: backgroundColor)),
          backgroundColor: colorCurve,
        ),
        body: SingleChildScrollView(
            child: esCamara == true ? camera() : galery()
             )
       );
          
           
  }

 Align galery(){
          return  Align(
              alignment: FractionalOffset.bottomCenter,
                child: Align(
                child: Image.file(imageGenerica),
             ),
          );
}

    Align camera(){
        return Align(
              alignment: FractionalOffset.bottomCenter,
                child: RotatedBox(
                  quarterTurns: 5,
                   child: Image.file(imageGenerica),
             ),
          );
      }
}