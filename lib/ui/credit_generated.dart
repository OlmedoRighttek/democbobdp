//import 'package:flutter/flare_actor.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/pin_code_fields.dart';
import 'package:myapp/utils/colors.dart' as prefix0;

import 'home_viwe.dart';


class CreditGenerated extends StatefulWidget {
  
  CreditGenerated();
  @override
  _CreditGenerated createState() =>
      _CreditGenerated();
  
}

class _CreditGenerated extends State<CreditGenerated> {
  var onTapRecognizer;

  TextEditingController textEditingController = TextEditingController();

  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
        Navigator.pop(context);
      };

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: scaffoldKey,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            children: <Widget>[
              SizedBox(height: 30),
              Container(
                height: MediaQuery.of(context).size.height / 3,
                child: FlareActor(
                  "assets/success_message.flr",
                  animation: "check-success",
                  fit: BoxFit.fitHeight,
                  alignment: Alignment.center,
                ),
              ),
              // Image.asset(
              //   'assets/verify.png',
              //   height: MediaQuery.of(context).size.height / 3,
              //   fit: BoxFit.fitHeight,
              // ),
              SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'Felicitaciones \n !El crédito ha sido aprobado¡',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              
         Row (mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[  
                       Container(
                          margin: const EdgeInsets.only(left: 50.0, right: 50.0),
                            child: new Image.asset(
                                "assets/icons/ico_tc.png",
                                width: 60,
                                height: 60,
                                )
                          ),
                      new Expanded(
                        child: new Text ("Se acreditará el monto a: \n Ricardo Antonio Marquez Enrrique \n Cuenta de ahorros 00XXXXXX05",   
                        textAlign: TextAlign.start,
                        style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.bold, 
                                color: Colors.grey, 
                                fontSize: 14),
                            ) ,
                      ), 
                    
                   ],
          ),
          SizedBox(height: 10),
        
    Row (
        mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: <Widget>[  
                  
                     Container(
                          margin: const EdgeInsets.only(left: 50.0, right: 50.0),
                            child: new Image.asset(
                                "assets/icons/ic_roles.png",
                                width: 60,
                                height: 60,
                                )
                          ),
                      new Expanded(
                        child: new Text ("Podrá retirar su dinero a tráves de: \n Los bancomático",   
                        textAlign: TextAlign.start,
                        style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.bold, 
                                color: Colors.grey, 
                                fontSize: 14),
                            ) ,
                      ), 
                    
                   ],
          ),
          SizedBox(height: 10),

        Row (mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[  
                       Container(
                            margin: const EdgeInsets.only(left: 50.0, right: 50.0),
                            child: new Image.asset(
                                "assets/icons/ic_desembolso_icon.png",
                                width: 60,
                                height: 60,
                                )
                          ),
                      new Expanded(
                        child: new Text ("Acerquese al Bancomático de su preferencia",   
                        textAlign: TextAlign.start,
                        style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.bold, 
                                color: Colors.grey, 
                                fontSize: 14),
                            ) ,
                      ), 
                    
                   ],
          ),
          SizedBox(height: 10),

          Row (mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[  
                      Container(
                          margin: const EdgeInsets.only(left: 50.0, right: 50.0),
                            child: new Image.asset(
                                "assets/icons/icon_onboarding_check.png",
                                width: 60,
                                height: 60,
                                )
                          ),
                      new Expanded(
                        child: new Text ("Gracias por confiar en nosotros, \n Vuelva prongo",   
                        textAlign: TextAlign.start,
                        style: TextStyle(
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.bold, 
                                color: Colors.grey, 
                                fontSize: 14),
                            ) ,
                      ), 
                    
                   ],
          ),
          
      
         // SizedBox(height: 10),
              Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 16.0, horizontal: 30),
                child: ButtonTheme(
                  height: 50,
                  child: FlatButton(
                    onPressed: () {
                     //  Navigator.pushNamed(context, '/second');
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeView()));
                     // Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));                      
                    },
                    child: Center(
                        child: Text(
                      "Regresar al inicio".toUpperCase(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    )),
                  ),
                ),
                decoration: BoxDecoration(
                    color: prefix0.colorCurve,
                    borderRadius: BorderRadius.circular(5),
                    // boxShadow: [
                    //   BoxShadow(
                    //       color: Colors.white,
                    //       offset: Offset(1, -2),
                    //       blurRadius: 5),
                    //   BoxShadow(
                    //       color: Colors.white,
                    //       offset: Offset(-1, 2),
                    //       blurRadius: 5)
                    // ]
                    ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
