
import 'package:flutter/material.dart';
import 'package:myapp/model/designCourseAppTheme.dart';
import 'package:myapp/ui/home_viwe.dart';
//import 'package:myapp/ui/page_login.dart';
import 'package:myapp/ui/request_infouser.dart';
import 'package:myapp/ui/simulador_credito.dart';
import 'package:myapp/utils/colors.dart';
import 'package:localstorage/localstorage.dart';
import 'package:myapp/widgets/fitnessAppHomeScreen.dart';
import 'balance_general.dart';
import 'cuentas_credito.dart';




class TransactionSteps extends StatefulWidget {
  int optionValidate = 0;
  TransactionSteps(this.optionValidate);
  @override
  //_TransactionSteps createState() => _TransactionSteps(this.optionValidate);
   State<StatefulWidget> createState() {
    return _TransactionSteps(this.optionValidate);
  }
}

class _TransactionSteps extends State<TransactionSteps>
    with TickerProviderStateMixin {

Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        //title: new Text('¿Estás seguro?'),
        content: new Text('¿Estás seguro que desea cancelar el proceso?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeView())),
            child: new Text('Si'),
          ),
        ],
      ),
    )) ?? false;
  }


  int optionValidate = 0;

  _TransactionSteps(this.optionValidate);
  final infoHeight = 364.0;
  AnimationController animationController;
  Animation<double> animation;
  var opacity1 = 0.0;
  var opacity2 = 0.0;
  var opacity3 = 0.0;
  
      final LocalStorage storage = new LocalStorage('todo_app');
  @override
  void initState() {
    animationController = AnimationController(
        duration: Duration(milliseconds: 1000), vsync: this);
    animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    super.initState();
  }

  void setData() async {
    animationController.forward();
    await Future.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    final tempHight = (MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        24.0);
    
     return new WillPopScope(
      onWillPop: _onWillPop,
      child: Container(
      color: colorCurve,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1.2,
                         // var a = storage.getItem('Img');

                  child: Image.asset("assets/images/emp_invertir_1.png"),
                ),
              ],
            ),
            Positioned(
              top: (MediaQuery.of(context).size.width / 1.2) - 24.0,
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                decoration: BoxDecoration(
                  color: DesignCourseAppTheme.nearlyWhite,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32.0),
                      topRight: Radius.circular(32.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: DesignCourseAppTheme.grey.withOpacity(0.2),
                        offset: Offset(1.1, 1.1),
                        blurRadius: 10.0),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  child: SingleChildScrollView(
                    child: Container(
                      constraints: BoxConstraints(
                          minHeight: infoHeight,
                          maxHeight:
                              tempHight > infoHeight ? tempHight : infoHeight),
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          AnimatedOpacity(
                            duration: Duration(milliseconds: 500),
                            opacity: opacity1,
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Column(
                              children: <Widget>[
                                optionValidate != 1  && optionValidate == 0 ?
                                 _listav2('1. IDENTIFICACIÓN', 1):
                                 _listav1('1. IDENTIFICACIÓN', 1),
                                optionValidate != 2 && optionValidate == 1 ?
                                 _listav2('2. INFORMACIÓN ECONÓMICA', 2):
                                 _listav1('2. INFORMACIÓN ECONÓMICA', 2),
                                optionValidate != 3 && optionValidate == 2 ?
                                 _listav2('3. SIMULACIÓN', 3):
                                 _listav1('3. SIMULACIÓN', 3),
                                optionValidate != 4 && optionValidate == 3 ?
                                 _listav2('4. CUENTA', 4):
                                 _listav1('4. CUENTA', 4),
                                optionValidate != 5 && optionValidate == 4 ?
                                 _listav2('5. CONTRATO', 5):
                                 _listav1('5. CONTRATO', 5),
                                 ],
                              ),
                            ),
                          ),
                        
                          AnimatedOpacity(
                            duration: Duration(milliseconds: 500),
                            opacity: opacity3,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 16, bottom: 16, right: 16),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      height: 48,
                   
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).padding.bottom,)
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
   
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: SizedBox(
                width: AppBar().preferredSize.height,
                height: AppBar().preferredSize.height,
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: new BorderRadius.circular(
                        AppBar().preferredSize.height),
                    child: Icon(
                      Icons.close,
                      color: Colors.white,
                    ),
                    onTap: () {
                      _neverSatisfied();
                      // AlertDialog(
                      //     title: Text("¿Está seguro de que desea cerrar esta sesión?"),
                      //     actions: [
                      //        FlatButton(
                      //           child: Text('No'),
                      //           onPressed: () {
                      //             Navigator.of(context).pop();
                      //           },
                      //         ),
                      //           FlatButton(
                      //           child: Text('No'),
                      //           onPressed: () {
                      //             Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));
                      //           },
                      //         ),
                      //     ],
                      //     elevation: 24.0,

                      // );
                      //   // Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    )
  
     );
    
    // return Container(
    //   color: colorCurve,
    //   child: Scaffold(
    //     backgroundColor: Colors.transparent,
    //     body: Stack(
    //       children: <Widget>[
    //         Column(
    //           children: <Widget>[
    //             AspectRatio(
    //               aspectRatio: 1.2,
    //                      // var a = storage.getItem('Img');

    //               child: Image.asset("assets/images/emp_invertir_1.png"),
    //             ),
    //           ],
    //         ),
    //         Positioned(
    //           top: (MediaQuery.of(context).size.width / 1.2) - 24.0,
    //           bottom: 0,
    //           left: 0,
    //           right: 0,
    //           child: Container(
    //             decoration: BoxDecoration(
    //               color: DesignCourseAppTheme.nearlyWhite,
    //               borderRadius: BorderRadius.only(
    //                   topLeft: Radius.circular(32.0),
    //                   topRight: Radius.circular(32.0)),
    //               boxShadow: <BoxShadow>[
    //                 BoxShadow(
    //                     color: DesignCourseAppTheme.grey.withOpacity(0.2),
    //                     offset: Offset(1.1, 1.1),
    //                     blurRadius: 10.0),
    //               ],
    //             ),
    //             child: Padding(
    //               padding: const EdgeInsets.only(left: 8, right: 8),
    //               child: SingleChildScrollView(
    //                 child: Container(
    //                   constraints: BoxConstraints(
    //                       minHeight: infoHeight,
    //                       maxHeight:
    //                           tempHight > infoHeight ? tempHight : infoHeight),
    //                   child: Column(
    //                     // mainAxisAlignment: MainAxisAlignment.center,
    //                     // crossAxisAlignment: CrossAxisAlignment.start,
    //                     children: <Widget>[
    //                       AnimatedOpacity(
    //                         duration: Duration(milliseconds: 500),
    //                         opacity: opacity1,
    //                         child: Padding(
    //                           padding: EdgeInsets.all(8),
    //                           child: Column(
    //                           children: <Widget>[
    //                             optionValidate != 1  && optionValidate == 1 ?
    //                              _listav2('1. IDENTIFICACIÓN', 1):
    //                              _listav1('1. IDENTIFICACIÓN', 1),
    //                             optionValidate != 2 && optionValidate == 1 ?
    //                              _listav2('2. INFORMACIÓN ECONÓMICA', 2):
    //                              _listav1('2. INFORMACIÓN ECONÓMICA', 2),
    //                             optionValidate != 3 && optionValidate == 2 ?
    //                              _listav2('3. SIMULACIÓN', 3):
    //                              _listav1('3. SIMULACIÓN', 3),
    //                             optionValidate != 4 && optionValidate == 3 ?
    //                              _listav2('4. CUENTA', 4):
    //                              _listav1('4. CUENTA', 4),
    //                             optionValidate != 5 && optionValidate == 4 ?
    //                              _listav2('5. CONTRATO', 5):
    //                              _listav1('5. CONTRATO', 5),
    //                              ],
    //                           ),
    //                         ),
    //                       ),
                        
    //                       AnimatedOpacity(
    //                         duration: Duration(milliseconds: 500),
    //                         opacity: opacity3,
    //                         child: Padding(
    //                           padding: const EdgeInsets.only(
    //                               left: 16, bottom: 16, right: 16),
    //                           child: Row(
    //                             mainAxisAlignment: MainAxisAlignment.center,
    //                             crossAxisAlignment: CrossAxisAlignment.center,
    //                             children: <Widget>[
    //                               Expanded(
    //                                 child: Container(
    //                                   height: 48,
                   
    //                                 ),
    //                               )
    //                             ],
    //                           ),
    //                         ),
    //                       ),
    //                       SizedBox(height: MediaQuery.of(context).padding.bottom,)
    //                     ],
    //                   ),
    //                 ),
    //               ),
    //             ),
    //           ),
    //         ),
   
    //         Padding(
    //           padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
    //           child: SizedBox(
    //             width: AppBar().preferredSize.height,
    //             height: AppBar().preferredSize.height,
    //             child: Material(
    //               color: Colors.transparent,
    //               child: InkWell(
    //                 borderRadius: new BorderRadius.circular(
    //                     AppBar().preferredSize.height),
    //                 child: Icon(
    //                   Icons.close,
    //                   color: Colors.white,
    //                 ),
    //                 onTap: () {
    //                   _neverSatisfied();
    //                   // AlertDialog(
    //                   //     title: Text("¿Está seguro de que desea cerrar esta sesión?"),
    //                   //     actions: [
    //                   //        FlatButton(
    //                   //           child: Text('No'),
    //                   //           onPressed: () {
    //                   //             Navigator.of(context).pop();
    //                   //           },
    //                   //         ),
    //                   //           FlatButton(
    //                   //           child: Text('No'),
    //                   //           onPressed: () {
    //                   //             Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));
    //                   //           },
    //                   //         ),
    //                   //     ],
    //                   //     elevation: 24.0,

    //                   // );
    //                   //   // Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));
    //                 },
    //               ),
    //             ),
    //           ),
    //         )
    //       ],
    //     ),
    //   ),
    // );
    
  }

Future<void> _neverSatisfied() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Cerrar sesión"),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('¿Está seguro de que desea cerrar esta sesión?'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('No'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
           FlatButton(
            child: Text('Si'),
            onPressed: () {
             Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeView()));

             // Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));
            },
          ),
        ],
      );
    },
  );
}

ListTile _listav1(String titulo, int number){
  return  ListTile(
          // leading: Image.asset(
         //   "assets/gif/touch_1.gif",
               // ),
                title: Text(titulo,
                style: TextStyle(fontSize: 20.0, color: colorCurve)),
                                    trailing: Row(      
                                        mainAxisSize:  MainAxisSize.min,    
                                        children: <Widget>[
                                      
                                      if(optionValidate == number || number <optionValidate)
                                           Icon(Icons.lock, size: 30.0,color: colorCurve)
                                      else
                                          Icon(Icons.lock_open, size: 30.0,color: colorCurve),

                                      if(optionValidate == number || number <optionValidate)
                                        Icon(Icons.check_circle , size: 30.0,color: colorCurve)
                                        
                                      ]),
                                    onTap: () {
                                      if(optionValidate == number-1)
                                       _navegationLis(number);
                                    },
                                    selected: true,
                                  );
}

ListTile _listav2(String titulo, int number){
  return  ListTile(
          leading: Image.asset(
           "assets/gif/touch_1.gif",
               ),
                title: Text(titulo,
                style: TextStyle(fontSize: 20.0, color: colorCurve)),
                                    trailing: Row(      
                                        mainAxisSize:  MainAxisSize.min,    
                                        children: <Widget>[
                                       optionValidate == number ? Icon(Icons.lock, size: 30.0,color: colorCurve):
                                      Icon(Icons.lock_open, size: 30.0,color: colorCurve),                                      //  Icon(Icons.check_circle , size: 30.0,color: colorCurve)
                                      ]),
                                    onTap: () {
                                      if(optionValidate == number - 1)
                                       _navegationLis(number);
                                    },
                                    selected: true,
                                  );
}

Widget tabBody = Container(
    color: Colors.red,
  );


void _navegationLis(int x){
  var navegarA;
     switch (x) {
       case 1 : 
         navegarA = RequestInfoUser(); //CreditGenerated(); //RequestInfoUser();
         break;
       case 2 : 
          navegarA = BalanceGeneral();
         break;
       case 3 : 
          navegarA = SimuladorCredito();
         break;
       case 4 : 
           navegarA = CuentaCredito();
         break;
       case 5 : 
         navegarA = FitnessAppHomeScreen(false);
         break;
       default:
     }
       Navigator.push(context, MaterialPageRoute(builder: (context) => navegarA));
}
  Widget getTimeBoxUI(String text1, String txt2) {
    return Padding(
      
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: DesignCourseAppTheme.nearlyWhite,
          borderRadius: BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: DesignCourseAppTheme.grey.withOpacity(0.2),
                offset: Offset(1.1, 1.1),
                blurRadius: 8.0),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                text1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: Colors.white,
                ),
              ),
              Text(
                txt2,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w200,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: DesignCourseAppTheme.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


