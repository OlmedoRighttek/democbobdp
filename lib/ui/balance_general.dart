
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myapp/ui/transaction_steps.dart';
import 'package:myapp/utils/acordeonDemo.dart';
//import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:myapp/utils/utils.dart';
import 'package:myapp/widgets/widgets.dart';
import 'package:myapp/utils/responsive_screen.dart';



void main() => runApp(new BalanceGeneral());

class BalanceGeneral extends StatefulWidget {
  BalanceGeneral({Key key}) : super(key: key);

  @override
  _BalanceGeneralInfo createState() => _BalanceGeneralInfo();
}

class _BalanceGeneralInfo extends State<BalanceGeneral> {
//  _formKey and _autoValidate
  var value;
  bool isExpanded = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = true;
  bool flag = true;
  // bool _isButtonDisabled = false; //true;
  // String _email;
  // String _mobile;
  Screen size;
  // String _myActivity;
  // File _imageFile;
  // dynamic _pickImageError;
  // bool _enabled = true;

  bool isLoading = false;

  
  @override
  Widget build(BuildContext context) {
        size = Screen(MediaQuery.of(context).size);

   return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Stack(children: <Widget>[
      
    SingleChildScrollView(
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20), vertical: size.getWidthPx(20)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                      children: [ 
                       new  IconButton(
                              icon: Icon(Icons.arrow_back,color: colorCurve),
                              onPressed: () => Navigator.pop(context, false),
                            ), 
                          new Expanded(
                              child: new GradientText ("Información economica", 
                                      gradient: LinearGradient(colors: [
                                      Color.fromRGBO(0, 94, 184, 1.0),  // fromRGBO(0, 94, 184), 
                                      Color.fromRGBO(0, 42, 102, 1.0)
                                    ]),
                                            style: TextStyle(
                                           fontFamily: 'Exo2', fontSize: 18, fontWeight: FontWeight.bold)
                                  ) ,
                            )
                   ],
                  ),
                    Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: FormUI(),
                        ),
                   ]),
          ),
        ),
      )
    ]));
 

  }

// Here is our Form UI
Widget FormUI() {
    return new Column(
      children: <Widget>[


       _helpX(),
     listado(),

       SizedBox(height: 20),

         _submitButtonWidget(),
      ],
    );
  }
  


Column listado(){
  return Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                left: 10.0,
                right: 10.0,
              ),
              child: Material(
                elevation: 5.0,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                child: GroovinExpansionTile(
                  defaultTrailingIconColor: colorCurve,
                  leading: CircleAvatar(
                    backgroundColor: colorCurve,
                    child: Icon(
                      Icons.attach_money,
                      color: Colors.white,
                    ),
                  ),
                  title: Text("BALANCE GENERAL DEL NEGOCIO PROPIO", style: TextStyle(color: Colors.black),),
                  //subtitle: Text(""),
                  onExpansionChanged: (value) {
                    setState(() {
                      isExpanded = value;
                    });
                  },
                  inkwellRadius: !isExpanded? BorderRadius.all(Radius.circular(8.0)): BorderRadius.only(
                    topRight: Radius.circular(8.0),
                    topLeft: Radius.circular(8.0),
                  ),
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(5.0),
                        bottomRight: Radius.circular(5.0),
                      ),
                      child: Column(
                             children: <Widget>[
                      SizedBox(height: 30),
           
              
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 0.0),
                child: Text(
                  'ACTIVOS',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(
                height: 10,
              ),
             
          Padding(padding: EdgeInsets.only(
                      left: 18.0, right: 18.0, top: 0.0, bottom: 0.0),    
                      child:  Theme(
                        child: TextFormField(
                        controller: TextEditingController(text: "500"),
                        autofocus: false,
                        autocorrect: false,
                        keyboardType: TextInputType.number,
                        inputFormatters: [WhitelistingTextInputFormatter(new RegExp(r'[+-]?([0-9]*[.])?[0-9]+'))],
                        decoration: const InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            border: const OutlineInputBorder(  borderRadius: const BorderRadius.all(
                             const Radius.circular(10.0),
                            )),
                            labelText: 'Bienes',
                            //prefixText: '\$ ',
                            prefixIcon: Icon(Icons.attach_money, 
                            color: Colors.blue,),
                            suffixText: '',
                            suffixStyle:
                            const TextStyle(color: Colors.green)
                            ),
                        maxLines: 1,
                        validator: (val) => val.isEmpty ? 'Monto requerido' : null,
                      ), 
                    
                         data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                        )
                          ),

          SizedBox(height: 10),
                   Row (
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[  
                                      new Padding(
                                    padding: const EdgeInsets.only(
                                    left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
                                        child: new Text ("Total Activos: \$ 500",   
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                                fontFamily: 'Exo2',
                                                fontWeight: FontWeight.bold, 
                                                color: Colors.grey, 
                                                fontSize: 18),
                                            ) ,
                                      ), 
                                  ],
                          ),
       
          SizedBox(height: 10),
       
          Padding(
                    padding: const EdgeInsets.symmetric(vertical: 0.0),
                    child: Text(
                      'PASIVOS',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                      textAlign: TextAlign.start,
                    ),
                  ),
            SizedBox(height: 10),


         
         
           Padding(padding: EdgeInsets.only( left: 18.0, right: 18.0, top: 0.0, bottom: 0.0), 
                         
              child:  Theme(
                        child: TextFormField(
                        autofocus: false,
                        autocorrect: false,
                        controller: TextEditingController(text: "200"),
                        keyboardType: TextInputType.number,
                        inputFormatters: [WhitelistingTextInputFormatter(new RegExp(r'[+-]?([0-9]*[.])?[0-9]+'))],
                        decoration: const InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            border: const OutlineInputBorder(  borderRadius: const BorderRadius.all(
                             const Radius.circular(10.0),
                            )),
                            labelText: 'Deudas',
                            //prefixText: '\$ ',
                            prefixIcon: Icon(Icons.attach_money, 
                            color: Colors.blue,),
                            suffixText: '',
                            suffixStyle:
                            const TextStyle(color: Colors.green)
                            ),
                        maxLines: 1,
                        validator: (val) => val.isEmpty ? 'Monto requerido' : null,
                      ), 
                    
                         data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                        )
                          ),

          
          SizedBox(height: 10),
                       Row (
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[  
                                      new Padding(
                                    padding: const EdgeInsets.only(
                                    left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
                                        child: new Text ("Total Pasivo: \$ 300",   
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                                fontFamily: 'Exo2',
                                                fontWeight: FontWeight.bold, 
                                                color: Colors.grey, 
                                                fontSize: 18),
                                            ) ,
                                      ), 
                                  ],
                          ),
                   new Divider(
                      color: Colors.grey,
                    ),
                   
                         Row (
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[  
                                      new Padding(
                                    padding: const EdgeInsets.only(
                                    left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
                                        child: new Text ("Total Patrimonio: \$ 800",   
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                                fontFamily: 'Exo2',
                                                fontWeight: FontWeight.bold, 
                                                color: Colors.grey, 
                                                fontSize: 18),
                                            ) ,
                                      ), 
                                  ],
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
         

         SizedBox(height: 20),  ///////////////////////////////////////////7


           Padding(
              padding: const EdgeInsets.only(
                left: 10.0,
                right: 10.0,
              ),
              child: Material(
                elevation: 5.0,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                child: GroovinExpansionTile(
                  defaultTrailingIconColor: colorCurve,
                  leading: CircleAvatar(
                    backgroundColor: colorCurve,
                    child: Icon(
                      Icons.attach_money,
                      color: Colors.white,
                    ),
                  ),
                  title: Text("PERDIDAS Y GANANCIAS NEGOCIO PROPIO", style: TextStyle(color: Colors.black),),
                  //subtitle: Text(""),
                  onExpansionChanged: (value) {
                    setState(() {
                      isExpanded = !value;
                    });
                  },
                  inkwellRadius: !isExpanded
                      ? BorderRadius.all(Radius.circular(8.0))
                      : BorderRadius.only(
                    topRight: Radius.circular(8.0),
                    topLeft: Radius.circular(8.0),
                  ),
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(5.0),
                        bottomRight: Radius.circular(5.0),
                      ),
                      child: Column(
                             children: <Widget>[
              SizedBox(height: 30),
           
              
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 0.0),
                child: Text(
                  'INGRESOS',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              
        
        
              Padding(padding: EdgeInsets.only(
                          left: 18.0, right: 18.0, top: 0.0, bottom: 0.0), 
                    child:  Theme(
                    child: TextFormField(
                        autofocus: false,
                        autocorrect: false,
                         controller: TextEditingController(text: "966"),
                        keyboardType: TextInputType.number,
                        inputFormatters: [WhitelistingTextInputFormatter(new RegExp(r'[+-]?([0-9]*[.])?[0-9]+'))],
                        decoration: const InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            border: const OutlineInputBorder(  borderRadius: const BorderRadius.all(
                             const Radius.circular(10.0),
                            )),
                            labelText: 'Ventas mensuales',
                            //prefixText: '\$ ',
                            prefixIcon: Icon(Icons.attach_money, 
                            color: Colors.blue,),
                            suffixText: '',
                            suffixStyle:
                            const TextStyle(color: Colors.green)
                            ),
                        maxLines: 1,
                        validator: (val) => val.isEmpty ? 'Monto requerido' : null,
                      ), 
                    
                         data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                        )
                          ),

          SizedBox(height: 10),

           Padding(padding: EdgeInsets.only( left: 18.0, right: 18.0, top: 0.0, bottom: 0.0), 
                         child:  Theme(
                        child: TextFormField(
                        autofocus: false,
                        autocorrect: false,
                        keyboardType: TextInputType.number,
                        controller: TextEditingController(text: "308"),
                        inputFormatters: [WhitelistingTextInputFormatter(new RegExp(r'[+-]?([0-9]*[.])?[0-9]+'))],
                        decoration: const InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            border: const OutlineInputBorder(  borderRadius: const BorderRadius.all(
                             const Radius.circular(10.0),
                            )),
                            labelText: 'Compras mensuales',
                            //prefixText: '\$ ',
                            prefixIcon: Icon(Icons.attach_money, 
                            color: Colors.blue,),
                            suffixText: '',
                            suffixStyle:
                            const TextStyle(color: Colors.green)
                            ),
                        maxLines: 1,
                        validator: (val) => val.isEmpty ? 'Monto requerido' : null,
                      ), 
                    
                         data: Theme.of(context)
                                  .copyWith(primaryColor: colorCurve, 
                          ),
                        )
                          ),

          
                         SizedBox(height: 10),
   
                         Row (
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[  
                                      new Padding(
                                    padding: const EdgeInsets.only(
                                    left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
                                        child: new Text ("Utilidad Bruta: \$ 658",   
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                                fontFamily: 'Exo2',
                                                fontWeight: FontWeight.bold, 
                                                color: Colors.grey, 
                                                fontSize: 18),
                                            ) ,
                                      ), 
                                  ],
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
         ],
        );
}




    Container _submitButtonWidget() {
    return Container(
      // padding: EdgeInsets.symmetric(
      // vertical: 20, horizontal: 16),
      width: 400,
      height: 60,
      child: RaisedButton(
        //isabledColor: _isButtonDisabled ? colorCurve : Colors.grey,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
        borderRadius: new  BorderRadius.circular(8.0)),
        //padding: EdgeInsets.all(12),
        child: Text(
          "Siguiente",
          style: TextStyle(
              fontFamily: 'Exo2', color: Colors.white, fontSize: 20.0),
        ),
        color: colorCurve,
        //onPressed: _validateInputs,
        onPressed : () {
           Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TransactionSteps(2)));

          // Navigator.push(context, MaterialPageRoute(builder: (context) => TransactionSteps(2)));
        } //_isButtonDisabled ? null : _validateInputs,
      ),
      
    );
  }



Row _helpX(){
      return Row (
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            new Expanded(
              child: new Text ("Con esta información se calculará el monto del crédito a otrorgar \n ",   style: TextStyle(
                      fontFamily: 'Exo2',
                      fontWeight: FontWeight.normal, 
                      color: Colors.grey, 
                      fontSize: 18),
                  ) ,
            ),  
            new  Image.asset(
                          "assets/images/hand.png",
                          fit: BoxFit.cover,
                          height: 200,
                ),    
          ],
      );
}


}