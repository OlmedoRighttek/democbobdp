import 'package:flutter/gestures.dart';
//import 'package:myapp/model/fintnessAppTheme.dart';
import 'package:open_appstore/open_appstore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:myapp/widgets/widgets.dart';
import 'package:myapp/utils/utils.dart';

import '../main.dart';

//import '../main.dart';
class SignUpPage extends StatefulWidget {
  const SignUpPage({Key key, this.mainScreenAnimation}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
    final Animation mainScreenAnimation;

}

class _SignUpPageState extends State<SignUpPage> {
  Screen size;
  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Stack(children: <Widget>[
        ClipPath(
        clipper: BottomShapeClipper(),
        child: Container(
          color: colorCurve,
        )),
          SingleChildScrollView(
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.getWidthPx(20), vertical: size.getWidthPx(20)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.arrow_back,color: colorCurve,),
                        onPressed: () => Navigator.pop(context, false),
                      ),
                      SizedBox(width: size.getWidthPx(20)),
                      _signUpGradientText(),
                    ],
                  ),
                  SizedBox(height: size.getWidthPx(20)),
                  //_socialButtons(),
                  _helpX(),
                  SizedBox(height: size.getWidthPx(30)),
                  //registerFields()
                  _lisHelp(),
                ]),
          ),
        ),
      )
    ]));
  }




  Column _lisHelp(){

  return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
           Text(
            "¿Tu usuario se bloqueó u olvidaste tu contraseña?",
            style: TextStyle(
                fontFamily: 'Exo2',
                fontWeight: FontWeight.bold, 
                color: Colors.black, 
                fontSize: 20),
             ),
             SizedBox(height: size.getWidthPx(10)),
                new Divider(
                  color: Colors.grey,
                ),
              SizedBox(height: size.getWidthPx(10)),

               Text(
            "Si se bloqueó tu usuario u olvidaste tu contraseña, Intermático ó Movilmático te ayudan a seguir usando nuestros productos y servicios.",
            style: TextStyle(
                fontFamily: 'Exo2',
                fontWeight: FontWeight.normal, 
                color: Colors.grey, 
                fontSize: 15),
             ),
              SizedBox(height: size.getWidthPx(10)),
             _submitButtonWidget(),
              SizedBox(height: size.getWidthPx(10)),
                _intermatico() ,
                SizedBox(height: size.getWidthPx(10)),
            _lisHelp2()
             
            ]

    );
  }




  Column _lisHelp2(){

  return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
           Text(
            "¿Necesitas asistencia personal?",
            style: TextStyle(
                fontFamily: 'Exo2',
                fontWeight: FontWeight.bold, 
                color: Colors.black, 
                fontSize: 20),
             ),
             SizedBox(height: size.getWidthPx(10)),
                new Divider(
                  color: Colors.grey,
                ),
              SizedBox(height: size.getWidthPx(10)),
             _bancaMovil(),
              SizedBox(height: size.getWidthPx(10)),

            ]
    );
  }




Container _bancaMovil() {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: size.getWidthPx(5), horizontal: size.getWidthPx(16)),
      width: size.getWidthPx(350),
      child: RaisedButton(
       // elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
        padding: EdgeInsets.all(size.getWidthPx(12)),
        child: Text(
          "Llamar a la Banca Telefónica",
          style: TextStyle(
              fontFamily: 'Exo2', color: colorCurve, fontSize: 20.0),
        ),
        color: HexColor("#edf1f4"),
        onPressed: _launchURL,
      ),
    );
  }


_launchURL() async {
  const tel = 'tel:043731500'; // tel:043731500
  if (await canLaunch(tel)) {
    await launch(tel);
  } else {
    throw 'Could not launch $tel';
  }
}

  RichText _intermatico() {
    return RichText(
      text: TextSpan(
          children: [
            TextSpan(
              style: TextStyle(color: Color.fromRGBO(0, 94, 184, 0.8),),
              text: 'Ir a Intermático',
              recognizer: TapGestureRecognizer()
                ..onTap = ()  async {
                    const url = 'https://www.intermatico.com/';
                    if (await canLaunch(url)) {
                      await launch(url, forceSafariVC: false);
                    } else {
                      throw 'No se pudo iniciar$url';
                    }
                  },
            ),
            
          ],
          style: TextStyle(color: Colors.black87, fontSize: 20.0, fontFamily: 'Exo2')),
    );
  }




Container _submitButtonWidget() {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: size.getWidthPx(20), horizontal: size.getWidthPx(16)),
      width: size.getWidthPx(350),
      child: RaisedButton(
       // elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
        padding: EdgeInsets.all(size.getWidthPx(12)),
        child: Text(
          "Abrir Banca Móvil",
          style: TextStyle(
              fontFamily: 'Exo2', color: colorCurve, fontSize: 20.0),
        ),
        color: HexColor("#edf1f4"),  //#edf1f4
        onPressed: () {
          // Redirigir Banca Móvil
           _dirigirTienda();
        },
      ),
    );
  }


  void _dirigirTienda() {
        // com.pacifico.movilmatico&hl=es    id880551607
        OpenAppstore.launch(androidAppId: "com.pacifico.movilmatico&hl=es", iOSAppId: "880551607");


  }




Row _helpX(){
 return Row (
    mainAxisAlignment: MainAxisAlignment.center,

    children: [
      new Expanded(
        child: new Text ("Sabemos que puedes tener ciertas dudas. Aqui trataremos de resolver algunas de ellas.",   style: TextStyle(
                fontFamily: 'Exo2',
                fontWeight: FontWeight.normal, 
                color: Colors.grey, 
                fontSize: 15),
             ) ,
      ),      
      new  Image.asset(
                    "assets/icons/ic_login_help.png",
                    fit: BoxFit.cover,
                    height: 50,
           ),
    ],
 
);
  
}


 Row _socialButtons() {
    return Row(
      
      mainAxisAlignment: MainAxisAlignment.spaceBetween,


      
      children: <Widget>[
        
        Column(
          
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
           Text(
            "¿Comó te ayudamos?",
            style: TextStyle(
                fontFamily: 'Exo2',
                fontWeight: FontWeight.bold, 
                color: Color.fromRGBO(0, 42, 102, 1.0), 
                fontSize: 20),
             ),
               Text(
            "Sabemos que puedes tener ciertas dudas. \nAqui trataremos de resolver algunas de ellas.",
            style: TextStyle(
                fontFamily: 'Exo2',
                fontWeight: FontWeight.normal, 
                color: Colors.grey, 
                fontSize: 15),
             )  
            ]
          ,),
     
        socialCircleAvatar("assets/icons/ic_login_help.png",(){}),
                SizedBox(width: size.getWidthPx(0)),

      ],
    );
  }


  GradientText _signUpGradientText() {
    return GradientText('Asistencia',
        gradient: LinearGradient(colors: [
          Color.fromRGBO(0, 94, 184, 1.0),  // fromRGBO(0, 94, 184), 
          Color.fromRGBO(0, 42, 102, 1.0)
        ]),
        style: TextStyle(fontFamily: 'Exo2',fontSize: 26, fontWeight: FontWeight.bold));
  }



  Container _signUpButtonWidget() {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: size.getWidthPx(20), horizontal: size.getWidthPx(16)),
      width: size.getWidthPx(200),
      child: RaisedButton(
        elevation: 8.0,
        shape: RoundedRectangleBorder(
            borderRadius:  BorderRadius.circular(30.0)),
        padding: EdgeInsets.all(size.getWidthPx(12)),
        child: Text(
          "Sign Up",
          style: TextStyle(fontFamily: 'Exo2',color: Colors.white, fontSize: 20.0),
        ),
        color: colorCurve,
        onPressed: () {
          // Going to DashBoard
        },
      ),
    );
  }

  GestureDetector socialCircleAvatar(String assetIcon, VoidCallback onTap) {
    return GestureDetector(
      onTap: onTap,
      child: CircleAvatar(
        maxRadius: size.getWidthPx(20),
        backgroundColor: Colors.white,
        child: Image.asset(assetIcon),
      ),
    );
  }

}
