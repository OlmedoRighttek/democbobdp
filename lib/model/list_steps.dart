import 'package:flutter/material.dart';

class ChatModel {
  final String avatarUrl;
  final String name;
  final String datetime;
  final String message;
  final IconData lockOpem;
  final IconData check;

  ChatModel({this.avatarUrl, this.name, this.datetime, this.message, this.lockOpem, this.check});

  static final List<ChatModel> dummyData = [
    ChatModel(
      avatarUrl: "https://randomuser.me/api/portraits/women/34.jpg",
      name: "1. IDENTIFICACIÓN",
      datetime: "20:18",
      message: "How about meeting tomorrow?",
      lockOpem: Icons.lock_open,
       check:Icons.cancel

    ),
    ChatModel(
      avatarUrl: "https://randomuser.me/api/portraits/women/49.jpg",
      name: "2. INFORMACIÓN ECONÓMICA",
      datetime: "19:22",
      message: "I love that idea, it's great!",
      lockOpem: Icons.lock_outline,
      check:Icons.check_circle,
    ),
    // ChatModel(
    //   avatarUrl: "https://randomuser.me/api/portraits/women/77.jpg",
    //   name: "Claire",
    //   datetime: "14:34",
    //   message: "I wasn't aware of that. Let me check",      
    //   lockOpem: Icons.lock_outline

    // ),
    // ChatModel(
    //   avatarUrl: "https://randomuser.me/api/portraits/men/81.jpg",
    //   name: "3. Simulación",
    //   datetime: "11:05",
    //   message: "Flutter just release 1.0 officially. Should I go for it?",      
    //   lockOpem: Icons.lock_outline

    // ),
    // ChatModel(
    //   avatarUrl: "https://randomuser.me/api/portraits/men/83.jpg",
    //   name: "4. Cuenta",
    //   datetime: "09:46",
    //   message: "It totally makes sense to get some extra day-off.",      
    //   lockOpem: Icons.lock_outline

    // ),
    // ChatModel(
    //   avatarUrl: "https://randomuser.me/api/portraits/men/85.jpg",
    //   name: "5. Contrato",
    //   datetime: "08:15",
    //   message: "It has been re-scheduled to next Saturday 7.30pm",      
    //   lockOpem: Icons.lock_outline

    // ),
  ];
}
