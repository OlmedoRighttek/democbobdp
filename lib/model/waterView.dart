//import 'dart:convert';

import 'package:flutter_html/flutter_html.dart';
//import 'package:myapp/model//waveView.dart';
import 'package:myapp/model/fintnessAppTheme.dart';
//import 'package:myapp/main.dart';
import 'package:flutter/material.dart';


class WaterView extends StatefulWidget {
  bool isAceptarTerminos = false;
  final AnimationController mainScreenAnimationController;
  final Animation mainScreenAnimation;

   WaterView(
      {Key key, this.mainScreenAnimationController, this.mainScreenAnimation, 
      this.isAceptarTerminos})
      : super(key: key);
  @override
  //_WaterViewState createState() => _WaterViewState();

    State<StatefulWidget> createState() {
    return _WaterViewState(this.isAceptarTerminos);
  }
}

class _WaterViewState extends State<WaterView> with TickerProviderStateMixin {
  bool isAceptarTerminos = false;
  _WaterViewState(this.isAceptarTerminos);
  Future<bool> getData() async {
    await Future.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.mainScreenAnimationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: widget.mainScreenAnimation,
          child: new Transform(
            transform: new Matrix4.translationValues(
                0.0, 30 * (1.0 - widget.mainScreenAnimation.value), 0.0),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 24, right: 24, top: 16, bottom: 18),
              child: Container(
                decoration: BoxDecoration(
                      color: FintnessAppTheme.white,
                      borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      bottomLeft: Radius.circular(8.0),
                      bottomRight: Radius.circular(8.0),
                      topRight: Radius.circular(68.0)),
                      boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: FintnessAppTheme.grey.withOpacity(0.2),
                                          offset: Offset(1.1, 1.1),
                                          blurRadius: 10.0),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 16, left: 16, right: 16, bottom: 16),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                // Row(
                                //   mainAxisAlignment: MainAxisAlignment.start,
                                //   crossAxisAlignment: CrossAxisAlignment.end,
                                //   children: <Widget>[
                                //     Padding(
                                //       padding: const EdgeInsets.only(
                                //           left: 4, bottom: 3),
                                //       child: Text(
                                //         '2100',
                                //         textAlign: TextAlign.center,
                                //         style: TextStyle(
                                //           fontFamily: FintnessAppTheme.fontName,
                                //           fontWeight: FontWeight.w600,
                                //           fontSize: 32,
                                //           color:
                                //               FintnessAppTheme.nearlyDarkBlue,
                                //         ),
                                //       ),
                                //     ),
                                //     Padding(
                                //       padding: const EdgeInsets.only(
                                //           left: 8, bottom: 8),
                                //       child: Text(
                                //         'ml',
                                //         textAlign: TextAlign.center,
                                //         style: TextStyle(
                                //           fontFamily: FintnessAppTheme.fontName,
                                //           fontWeight: FontWeight.w500,
                                //           fontSize: 18,
                                //           letterSpacing: -0.2,
                                //           color:
                                //               FintnessAppTheme.nearlyDarkBlue,
                                //         ),
                                //       ),
                                //     ),
                                //   ],
                                // ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 4, top: 2, bottom: 14),
                                  child: Html(
                                    data: 
                                    this.isAceptarTerminos == true ?
                                    """
                                    El solicitante declara y acepta que est&aacute;n conformes con todas y cada una de las siguientes cl&aacute;usulas especificadas en\n    este documento:</p>\n<p align=\"justify\">Usted debe ingresar informaci&oacute;n real y fidedigna, sustentada y declarada de todos los intervinientes del cr&eacute;dito; la misma\n    que ser&aacute; verificada con los respectivos soportes y ser&aacute; estrictamente confidencial. No suplantando la identidad de ninguna\n    persona.</p>\n<p align=\"justify\">Para poder evaluar su record de pago en el sistema financiero, es necesario que usted nos autorice a consultar su informaci&oacute;n\n    a la Direcci&oacute;n Nacional de Registro de Datos P&uacute;blicos, a entidades que mantengan registros de informaci&oacute;n crediticia\n    o cualquier otra entidad debidamente autorizada de conformidad con la Ley, para la conservaci&oacute;n y entrega de dicha informaci&oacute;n,\n    bastando para este efecto que usted se identifique plenamente por el presente medio y de su se&ntilde;al de aceptaci&oacute;n en estas\n    condiciones.</p>\n\n<p align=\"justify\">Cr&eacute;dito Banca de oportunidades realiza un proceso de evaluaci&oacute;n autom&aacute;tica, cuyo resultado se encuentran en funci&oacute;n de las condiciones\n    establecidas para este tipo de cr&eacute;dito y est&aacute; sujeto a restricciones, las cuales le ser&aacute;n comunicadas de ser el caso,\n    por parte del Banco.,
                                    """
                                    : "El solicitante declara y acepta que est&aacute;n conformes con todas y cada una de las siguientes cl&aacute;usulas especificadas en\n    este documento:</p>\n<p align=\"justify\">Usted debe ingresar informaci&oacute;n real y fidedigna, sustentada y declarada de todos los intervinientes del cr&eacute;dito; la misma\n    que ser&aacute; verificada con los respectivos soportes y ser&aacute; estrictamente confidencial. No suplantando la identidad de ninguna\n    persona.</p>\n<p align=\"justify\">Para poder evaluar su record de pago en el sistema financiero, es necesario que usted nos autorice a consultar su informaci&oacute;n\n    a la Direcci&oacute;n Nacional de Registro de Datos P&uacute;blicos, a entidades que mantengan registros de informaci&oacute;n crediticia\n    o cualquier otra entidad debidamente autorizada de conformidad con la Ley, para la conservaci&oacute;n y entrega de dicha informaci&oacute;n,\n    bastando para este efecto que usted se identifique plenamente por el presente medio y de su se&ntilde;al de aceptaci&oacute;n en estas\n    condiciones.</p>\n\n<p align=\"justify\">Cr&eacute;dito Banca de oportunidades realiza un proceso de evaluaci&oacute;n autom&aacute;tica, cuyo resultado se encuentran en funci&oacute;n de las condiciones\n    establecidas para este tipo de cr&eacute;dito y est&aacute; sujeto a restricciones, las cuales le ser&aacute;n comunicadas de ser el caso,\n    por parte del Banco.,",
                                    // textAlign: TextAlign.center,
                                    // style: TextStyle(
                                    //   fontFamily: FintnessAppTheme.fontName,
                                    //   fontWeight: FontWeight.w500,
                                    //   fontSize: 14,
                                    //   letterSpacing: 0.0,
                                    //   color: FintnessAppTheme.darkText,
                                    // ),
                                  ),
                                ),
                              ],
                            ),
                            // Padding(
                            //   padding: const EdgeInsets.only(
                            //       left: 4, right: 4, top: 8, bottom: 16),
                            //   child: Container(
                            //     height: 2,
                            //     decoration: BoxDecoration(
                            //       color: FintnessAppTheme.background,
                            //       borderRadius:
                            //           BorderRadius.all(Radius.circular(4.0)),
                            //     ),
                            //   ),
                            // ),
                            // Padding(
                            //   padding: const EdgeInsets.only(top: 16),
                            //   child: Column(
                            //     mainAxisAlignment: MainAxisAlignment.center,
                            //     crossAxisAlignment: CrossAxisAlignment.end,
                            //     children: <Widget>[
                            //       Row(
                            //         mainAxisAlignment: MainAxisAlignment.start,
                            //         crossAxisAlignment:
                            //             CrossAxisAlignment.center,
                            //         children: <Widget>[
                            //           Padding(
                            //             padding: const EdgeInsets.only(left: 4),
                            //             child: Icon(
                            //               Icons.access_time,
                            //               color: FintnessAppTheme.grey
                            //                   .withOpacity(0.5),
                            //               size: 16,
                            //             ),
                            //           ),
                            //           Padding(
                            //             padding:
                            //                 const EdgeInsets.only(left: 4.0),
                            //             child: Text(
                            //               'Last drink 8:26 AM',
                            //               textAlign: TextAlign.center,
                            //               style: TextStyle(
                            //                 fontFamily:
                            //                     FintnessAppTheme.fontName,
                            //                 fontWeight: FontWeight.w500,
                            //                 fontSize: 14,
                            //                 letterSpacing: 0.0,
                            //                 color: FintnessAppTheme.grey
                            //                     .withOpacity(0.5),
                            //               ),
                            //             ),
                            //           ),
                            //         ],
                            //       ),
                            //       Padding(
                            //         padding: const EdgeInsets.only(top: 4),
                            //         child: Row(
                            //           mainAxisAlignment:
                            //               MainAxisAlignment.start,
                            //           crossAxisAlignment:
                            //               CrossAxisAlignment.center,
                            //           children: <Widget>[
                            //             SizedBox(
                            //               width: 24,
                            //               height: 24,
                            //               child: Image.asset(
                            //                   "assets/fitness_app/bell.png"),
                            //             ),
                            //             Flexible(
                            //               child: Text(
                            //                 'Your bottle is empty, refill it!.',
                            //                 textAlign: TextAlign.start,
                            //                 style: TextStyle(
                            //                   fontFamily:
                            //                       FintnessAppTheme.fontName,
                            //                   fontWeight: FontWeight.w500,
                            //                   fontSize: 12,
                            //                   letterSpacing: 0.0,
                            //                   color: HexColor("#F65283"),
                            //                 ),
                            //               ),
                            //             ),
                            //           ],
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // )
                          ],
                        ),
                      ),
                      // SizedBox(
                      //   width: 34,
                      //   child: Column(
                      //     mainAxisAlignment: MainAxisAlignment.center,
                      //     crossAxisAlignment: CrossAxisAlignment.center,
                      //     children: <Widget>[
                      //       Container(
                      //         decoration: BoxDecoration(
                      //           color: FintnessAppTheme.nearlyWhite,
                      //           shape: BoxShape.circle,
                      //           boxShadow: <BoxShadow>[
                      //             BoxShadow(
                      //                 color: FintnessAppTheme.nearlyDarkBlue
                      //                     .withOpacity(0.4),
                      //                 offset: Offset(4.0, 4.0),
                      //                 blurRadius: 8.0),
                      //           ],
                      //         ),
                      //         child: Padding(
                      //           padding: const EdgeInsets.all(6.0),
                      //           child: Icon(
                      //             Icons.add,
                      //             color: FintnessAppTheme.nearlyDarkBlue,
                      //             size: 24,
                      //           ),
                      //         ),
                      //       ),
                      //       SizedBox(
                      //         height: 28,
                      //       ),
                      //       Container(
                      //         decoration: BoxDecoration(
                      //           color: FintnessAppTheme.nearlyWhite,
                      //           shape: BoxShape.circle,
                      //           boxShadow: <BoxShadow>[
                      //             BoxShadow(
                      //                 color: FintnessAppTheme.nearlyDarkBlue
                      //                     .withOpacity(0.4),
                      //                 offset: Offset(4.0, 4.0),
                      //                 blurRadius: 8.0),
                      //           ],
                      //         ),
                      //         child: Padding(
                      //           padding: const EdgeInsets.all(6.0),
                      //           child: Icon(
                      //             Icons.remove,
                      //             color: FintnessAppTheme.nearlyDarkBlue,
                      //             size: 24,
                      //           ),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // Padding(
                      //   padding:
                      //       const EdgeInsets.only(left: 16, right: 8, top: 16),
                      //   child: Container(
                      //     width: 60,
                      //     height: 160,
                      //     decoration: BoxDecoration(
                      //       color: HexColor("#E8EDFE"),
                      //       borderRadius: BorderRadius.only(
                      //           topLeft: Radius.circular(80.0),
                      //           bottomLeft: Radius.circular(80.0),
                      //           bottomRight: Radius.circular(80.0),
                      //           topRight: Radius.circular(80.0)),
                      //       boxShadow: <BoxShadow>[
                      //         BoxShadow(
                      //             color: FintnessAppTheme.grey.withOpacity(0.4),
                      //             offset: Offset(2, 2),
                      //             blurRadius: 4),
                      //       ],
                      //     ),
                      //     child: WaveView(),
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
