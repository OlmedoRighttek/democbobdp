import 'package:myapp/ui/page_login.dart';

class HotelListData {
  String imagePath;
  String icon;
  String titleTxt;
  String subTxt;
  double dist;
  double rating;
  int reviews;
  int perNight;
  dynamic navegationPage;
  bool isNewNavegation;
  String urlStore;
  String descriptionInfo;
  String requisitos;
  String infoCards;


  HotelListData({
    this.infoCards = '',
    this.urlStore = '',
    this.navegationPage = LoginPage,
    this.isNewNavegation = false,
    this.icon = '',
    this.imagePath = '',
    this.titleTxt = '',
    this.subTxt = '',
    this.descriptionInfo = '',
    this.requisitos = '',
  });


  static List<HotelListData> hotelList = [
    HotelListData(
      infoCards: '',
      requisitos: '',
      descriptionInfo: 'Genere su propico credito de forma sencilla',
      isNewNavegation: false,
      urlStore:'', //androidAppId: "com.pacifico.movilmatico&hl=es", iOSAppId: "880551607",
      icon: 'assets/icon_prod_ahorros.png',
      imagePath: 'assets/hotel/banner_home_ahorro.png',
      titleTxt: 'Solicitar Credito',
      subTxt: 'Crédito sencillo sin intereses',

    ),

    HotelListData(
      infoCards: 'Mejora tus ingresos incrementando tu volumen de compra.| Mejora el abastecimiento de tus productos. | Financiamiento a bajo costo. | Tasa preferencial. | Sin garante. | Con seguro de desgravamen.',
      requisitos: '\n 1. Tener una cuenta activa en Banco del Pacifico. \n 2. Tener usuario y contraseña activa de Intermático. \n 3. Tener a mano su cédula de identidad y planilla de servicios básicos mas reciente',
      descriptionInfo: 'A través de Banca de Oportunidades BdP Banco del Pacífico te abre las puertas a Para que puedas emprender y hacer crecer tu negocio, de una manera sencilla, de una manera digital',
      isNewNavegation: true,
      urlStore:'', 
      icon: 'assets/icon_prod_creditcard.png',
      imagePath: 'assets/ic_coche_bg.png', //'assets/hotel/banner_home_inversiones.png',
      titleTxt: 'Banca Oportunidades',
      subTxt: 'Te abre las puertas a Para que puedas emprender y hacer crecer tu negocio, de una manera sencilla.',
    ),

    HotelListData(
      infoCards: 'Monto mínimo \$300, máximo \$500. | Tasa de interés del 15%. | Desde 3 meses hasta 24 meses de financiamiento. | Sin garantes. | Con seguro de desgravamen.',
      requisitos: '\n 1. Manejar actualmente crédito con la Empresa Ancla \n 2. Tener una cuenta activa en Banco del Pacifico. \n 3. Tener usuario y contraseña activa de Intermático.',
      descriptionInfo: 'Banco del Pacífico en pro de generar más beneficios y facilidades para tu negocio, ha trabajado en Banca Retail BdP, una respuesta de financiamiento para la adquisición de tus insumos/compras, permitiendo un mayor plazo para tus pagos.',
      isNewNavegation: true,
      urlStore:'com.bancodelpacifico.BancaRetail&hl=es 880551607', 
      icon: 'assets/icon_prod_inversiones.png',
      imagePath: 'assets/ic_coche_bg.png',
      titleTxt: 'BdP Retail',
      subTxt: 'Financiamiento de tus insumos/compras mediante un credito.',
    ),

     HotelListData(
      infoCards: '',
      requisitos: '',
      descriptionInfo: 'Permite que usted generé su propio credito en base a su información crediticio',
      isNewNavegation: true,
      urlStore:'com.desa.hiper.microfinanzas&hl=es 880551607', 
      icon: 'assets/icon_prod_ahorros.png',
      imagePath: 'assets/ic_coche_bg.png',
      titleTxt: 'Banca Social',
      subTxt: 'Solicita un credito',
    ),
  ];
}
