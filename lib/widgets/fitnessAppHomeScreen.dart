import 'package:myapp/model/tabIconData.dart';
import 'package:flutter/material.dart';
import 'package:myapp/model/fintnessAppTheme.dart';
//import 'package:myapp/model/trainingScreen.dart';
import 'package:myapp/ui/accept_terms.dart';
// import 'package:myapp/ui/start_request.dart';
import 'bottomBarView.dart';

class FitnessAppHomeScreen extends StatefulWidget {
   bool isAceptarTerminos = false;
   FitnessAppHomeScreen(this.isAceptarTerminos);
  @override
 State<StatefulWidget> createState() {
    return _FitnessAppHomeScreenState(this.isAceptarTerminos);
  }

 // _FitnessAppHomeScreenState createState() => _FitnessAppHomeScreenState();
}

class _FitnessAppHomeScreenState extends State<FitnessAppHomeScreen>
  with TickerProviderStateMixin {
   bool isAceptarTerminos = false;
  _FitnessAppHomeScreenState(this.isAceptarTerminos);

  AnimationController animationController;

  List<TabIconData> tabIconsList = TabIconData.tabIconsList;

  Widget tabBody = Container(
    color: FintnessAppTheme.background,
  );

  @override
  void initState() {
    tabIconsList.forEach((tab) {
      tab.isSelected = false;
    });
    tabIconsList[0].isSelected = true;

    animationController =
        AnimationController(duration: Duration(milliseconds: 600), vsync: this);
        
    tabBody = MyDiaryScreen(animationController: animationController, isAceptarTerminos: isAceptarTerminos );
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: FintnessAppTheme.background,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return SizedBox();
            } else {
              return Stack(
                children: <Widget>[
                  tabBody,
                  bottomBar(this.isAceptarTerminos),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  Future<bool> getData() async {
    await Future.delayed(const Duration(milliseconds: 200));
    return true;
  }

  Widget bottomBar(bool isAceptarTerminos) {
    return Column(
      children: <Widget>[
        Expanded(
          child: SizedBox(),
        ),
        BottomBarView(
          isAceptarTerminos : isAceptarTerminos,
          tabIconsList: tabIconsList,
          addClick: () {},
          changeIndex: (index) {
            if (index == 0) {
              animationController.reverse().then((data) {
                if (!mounted) return;
                setState(() {
                  tabBody =
                      MyDiaryScreen(animationController: animationController, isAceptarTerminos:isAceptarTerminos);
                });
              });
            } else{
              animationController.reverse().then((data) {
                // if (!mounted) return;
                // setState(() {
                //   tabBody =
                //        CourseInfoScreen();
                //       //TrainingScreen(animationController: animationController);
                // });
              });
            }
          },
        ),
      ],
    );
  }
}
