import 'package:flutter/material.dart';

class InfoCard extends StatelessWidget {
  final String photo;

  const InfoCard({Key key, @required this.photo}) : super(key: key);
  @override
  Widget build(BuildContext context) {

    final BorderRadius br = BorderRadius.circular(20.0);

    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0, right: 10.0, top: 10, left: 10),
      child: Material(
        borderRadius: br,
            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(16.0)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: Colors.grey,
                                      offset: Offset(1.1, 1.1),
                                      blurRadius: 8.0),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 60.0, right: 60.0, top: 0.0, bottom: 0.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "text1",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 14,
                                        letterSpacing: 0.27,
                                        color: Colors.blue,
                                      ),
                                    ),
                                    Text(
                                      "txt2",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                        fontSize: 14,
                                        letterSpacing: 0.27,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                       ),
    );
  }
}
