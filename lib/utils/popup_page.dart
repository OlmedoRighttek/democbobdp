
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:myapp/model/fintnessAppTheme.dart';
import 'package:myapp/utils/responsive_screen.dart';

class PopupPage extends StatefulWidget {
  String requisitos = '';
  PopupPage(this.requisitos);
  @override
     State<StatefulWidget> createState() {
    return _PopupPage(this.requisitos);
  }
}

class _PopupPage extends State<PopupPage> {
  Screen size;
    String requisitos = '';
  _PopupPage(this.requisitos);

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      //    appBar: new AppBar(
      //      backgroundColor: Colors.white,
      //   //title: new Text('Page Title'),
      //   actions: <Widget>[
      //      new Container(
      //     width: 250,
      //     height: 250,
      //       child: FlareActor(
      //             "assets/error_ans.flr",
      //             animation: "Untitled",
      //             fit: BoxFit.fitHeight,
      //             alignment: Alignment.center,
      //           ),),
      //    ],
      //   leading: new Container(),
      // ),
        body: Column(
            children: <Widget>[ 
            Container(
            height: 110,
              child: GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: FlareActor(
                        "assets/error_ans.flr",
                        animation: "Untitled",
                        fit: BoxFit.fitHeight,
                        alignment: Alignment.bottomRight,
                      ),     
                )
             ),
            Container(
                height: 200,
                width: 200,
                  child: FlareActor(
                      "assets/alert.flr",
                      //animation: "show",
                      fit: BoxFit.fitHeight,
                      alignment: Alignment.bottomRight,
                    ),     
             ),

             Padding(
                    padding: const EdgeInsets.only(left: 8, top: 0, right: 8),
                            child: Text(
                              'REQUISITOS',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: FintnessAppTheme.fontName,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 32,
                                  letterSpacing: -0.1,
                                  color: FintnessAppTheme.darkText),
                            ),
             ),

             Padding(
                    padding: const EdgeInsets.only(left: 30, top: 0, right: 30),
                    child:  Text(
                                      requisitos,
                                      textAlign: TextAlign.justify,
                                      style: TextStyle(
                                                    fontFamily: FintnessAppTheme.fontName,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 18,
                          )
                    )

                   )
           

              ],

        ), 
                
    );

    // Text listRequisitos(){
    //  if (requisitos != '') {
    //     requisitos.split('|');



    //    return Text(
    //                 '\n 1- Cédula de indentidad \n 2- Planilla de servicios basicos',
    //                 textAlign: TextAlign.justify,
    //                 style: TextStyle(
    //                               fontFamily: FintnessAppTheme.fontName,
    //                               fontWeight: FontWeight.w500,
    //                               fontSize: 23,
    //     )
    //     );
    //  } else {
    //    return Text(
    //                 'No existen requisitos disponibles',
    //                 textAlign: TextAlign.justify,
    //                 style: TextStyle(
    //                               fontFamily: FintnessAppTheme.fontName,
    //                               fontWeight: FontWeight.w500,
    //                               fontSize: 23,
    //     )
    //     );
    //  } 
    // }
  }

}