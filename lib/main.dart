import 'package:flutter/material.dart';
import 'package:myapp/ui/home_viwe.dart';
//import 'package:flutter/services.dart';
//import 'package:flutter_ui_collections/ui/page_splash.dart';
import 'ui/page_splash.dart';
// import 'ui/page_home.dart';
// import 'ui/page_login.dart';
// import 'ui/page_onboarding.dart';
// import 'ui/page_settings.dart';
// import 'utils/utils.dart';
// import 'widgets/carousol.dart';
import 'package:flutter/services.dart';


void main() async  {
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  //runApp(MyApp());


  runApp(
    MaterialApp(
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
    '/': (context) => MyApp(),
    '/second': (context) => HomeView(),
  },
    ),
  );
  
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter UI Collections',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: CollectionApp(),

    );
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class CollectionApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Theme.of(context).primaryColor,
        ),
        home: SplashScreen()
    );
  }
}


